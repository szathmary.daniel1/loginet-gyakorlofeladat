import { useState } from "react";

export default function useYoutubeApiPagination(youtubeApiGetRequest) {
	const [nextPageToken, setNextPageToken] = useState("");
	const [prevPageToken, setPrevPageToken] = useState("");

	const firstPage = async (...args) => {
		setNextPageToken("");
		setPrevPageToken("");

		const data = await youtubeApiGetRequest(...args);

		setNextPageToken(data.nextPageToken ?? "");

		return data;
	};

	const nextPage = async (...args) => {
		const data = await youtubeApiGetRequest(...args, nextPageToken);

		setNextPageToken(data.nextPageToken ?? "");
		setPrevPageToken(data.prevPageToken ?? "");

		return data;
	};

	const prevPage = async (...args) => {
		const data = await youtubeApiGetRequest(...args, prevPageToken);

		setNextPageToken(data.nextPageToken ?? "");
		setPrevPageToken(data.prevPageToken ?? "");

		return data;
	};

	const hasNext = nextPageToken !== "";
	const hasPrev = prevPageToken !== "";

	return {
		hasNext,
		hasPrev,
		nextPage,
		prevPage,
		firstPage,
	};
}
