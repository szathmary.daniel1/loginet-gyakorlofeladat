import { useEffect, useState } from "react";
import { Link, useNavigate, useParams } from "react-router-dom";
import useYoutubeApiPagination from "../../hooks/useYoutubeApiPagination";
import youtubeApi from "../../api/youtubeApi";
import VideoGrid from "../../components/VideoGrid";
import VideoInfoBox from "./VideoInfoBox";
import PaginationButtons from "../../components/PaginationButtons";

export default function VideoPage() {
	const { getVideoInfosById, getRelatedToVideoId } = youtubeApi;
	
	const { videoId } = useParams();
	const navigate = useNavigate();
	const { hasNext, hasPrev, nextPage, prevPage, firstPage } =
		useYoutubeApiPagination(getRelatedToVideoId);

	const [relatedVideos, setRelatedVideos] = useState([]);
	const [videoInfo, setVideoInfo] = useState({
		id: videoId,
		publishedAt: "",
		channelId: "",
		title: "",
		description: "",
		thumbnail: "",
		channelTitle: "",
		viewCount: 0,
		likeCount: 0,
		dislikeCount: 0,
	});

	useEffect(() => {
		const getVideoInfo = async () => {
			const data = await getVideoInfosById(videoId);

			if (data.pageInfo.totalResults === 0)
				return navigate("/", { replace: true });

			const { publishedAt, channelId, title, description, channelTitle } =
				data.items[0].snippet;
			const thumbnail = data.items[0].snippet.thumbnails.medium.url;
			const { viewCount, likeCount, commentCount } = data.items[0].statistics;

			setVideoInfo({
				publishedAt: new Date(publishedAt).toDateString(),
				channelId,
				title,
				description,
				thumbnail,
				channelTitle,
				viewCount,
				likeCount,
				commentCount,
			});
		};
		getVideoInfo();
	}, [getVideoInfosById, navigate, videoId]);

	/* eslint-disable */
	useEffect(() => {
		const getVideos = async () => {
			const data = await firstPage(videoId);
			const newRelatedVideos = relatedVideoDataMapper(data);
			setRelatedVideos(newRelatedVideos);
		};

		getVideos();
	}, [videoId]);
	/* eslint-enable */

	const handleNextPageClick = async () => {
		if (!hasNext) return;
		const newRelatedVideos = relatedVideoDataMapper(await nextPage(videoId));
		setRelatedVideos(newRelatedVideos);
	};

	const handlePrevPageClick = async () => {
		if (!hasPrev) return;
		const newRelatedVideos = relatedVideoDataMapper(await prevPage(videoId));
		setRelatedVideos(newRelatedVideos);
	};

	return (
		<div className="container-page-video">
			<Link to="/" replace={true}>
				<i className="fa-solid fa-arrow-left"></i> Back to home
			</Link>
			<VideoInfoBox videoInfo={videoInfo} />
			<div>
				<h1>Related Videos</h1>
				<VideoGrid videos={relatedVideos} />
				<PaginationButtons
					hasNext={hasNext}
					hasPrev={hasPrev}
					onNext={handleNextPageClick}
					onPrev={handlePrevPageClick}
				/>
			</div>
		</div>
	);
}

const relatedVideoDataMapper = (data) =>
	data.items.map((item) => {
		const id = item.id.videoId;
		const { description, publishedAt, title } = item.snippet;
		const thumbnail = item.snippet.thumbnails.medium.url;

		return {
			id,
			description,
			publishedAt: new Date(publishedAt).toDateString(),
			title,
			thumbnail,
		};
	});
