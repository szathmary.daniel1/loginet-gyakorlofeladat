import { useState } from "react";
import { useNavigate } from "react-router-dom";
import useYoutubeApiPagination from "../../hooks/useYoutubeApiPagination";
import youtubeApi from "../../api/youtubeApi";
import PaginationButtons from "../../components/PaginationButtons";
import Searchbar from "../../components/Searchbar";
import ChannelList from "./ChannelList";

export default function HomePage() {
	const { getChannelsByQuery } = youtubeApi;
	
	const navigate = useNavigate();
	const { hasNext, hasPrev, nextPage, prevPage, firstPage } =
		useYoutubeApiPagination(getChannelsByQuery);

	const [query, setQuery] = useState("");
	const [channels, setChannels] = useState([]);

	const handleSearch = async (query) => {
		setQuery(query);
		if (query === "") {
			setChannels([]);
			return;
		}

		const newChannels = channelDataMapper(await firstPage(query));

		setChannels(newChannels);
	};

	const handleChannelItemClick = (channelId) => {
		navigate(`/channel/${channelId}`, { replace: true });
	};

	const handleNextPageClick = async () => {
		if (!hasNext) return;
		const newChannels = channelDataMapper(await nextPage(query));
		setChannels(newChannels);
	};

	const handlePrevPageClick = async () => {
		if (!hasPrev) return;
		const newChannels = channelDataMapper(await prevPage(query));
		setChannels(newChannels);
	};

	return (
		<div className="container-page-home">
			<Searchbar onSearch={handleSearch}></Searchbar>
			<ChannelList channels={channels} onClick={handleChannelItemClick} />
			{channels.length > 0 ? (
				<PaginationButtons
					onNext={handleNextPageClick}
					onPrev={handlePrevPageClick}
					hasNext={hasNext}
					hasPrev={hasPrev}
				/>
			) : null}
		</div>
	);
}

const channelDataMapper = (data) => data.items.map((item) => item.snippet);
