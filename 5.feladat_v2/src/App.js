import { createBrowserRouter, RouterProvider } from "react-router-dom";
import MainLayout from "./layouts/MainLayout";
import ChannelPage from "./pages/ChannelPage/ChannelPage";
import HomePage from "./pages/HomePage/HomePage";
import VideoPage from "./pages/VideoPage/VideoPage";

export default function App() {
	const router = createBrowserRouter([
		{
			path: "/",
			element: <MainLayout />,
			children: [
				{
					index: true,
					element: <HomePage />,
				},
				{
					path: "channel/:channelId",
					element: <ChannelPage />,
				},
				{
					path: "/video/:videoId",
					element: <VideoPage />,
				},
			],
		},
	]);
	return <RouterProvider router={router} />;
}
