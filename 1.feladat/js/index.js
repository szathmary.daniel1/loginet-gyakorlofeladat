const userStatChartDiv = document.querySelector("#user-stat .bar-chart");
const statisticChartDiv = document.querySelector("#statistic .bar-chart");
const siteSpeedDiv = document.querySelector("#site-speed .site-speed");

const cardTabSelects = document.querySelectorAll(
	".card .card-header .card-tab-select select"
);

const userStatTabs = document.querySelectorAll(
	"#user-stat .card-header .card-tabs .card-tab"
);

const statisticTabs = document.querySelectorAll(
	"#statistic .card-header .card-tabs .card-tab"
);

const siteSpeedTabs = document.querySelectorAll(
	"#site-speed .card-header .card-tabs .card-tab"
);

const userStatState = {
	isVertical: true,
	charts: {
		month: {
			values: Array(22)
				.fill(0)
				.map((item) => Math.floor(Math.random() * 100)),
			xLabels: Array(11)
				.fill(0)
				.map((item, index) => 9 + index * 2),
			yLabels: Array(7)
				.fill(0)
				.map((item, index) => 0 + index * 100)
				.reverse(),
		},
		sixMonth: {
			values: Array(6)
				.fill(0)
				.map((item) => Math.floor(Math.random() * 100)),
			xLabels: Array(6)
				.fill(0)
				.map((item, index) => index + 1),
			yLabels: Array(7)
				.fill(0)
				.map((item, index) => 0 + index * 100)
				.reverse(),
		},
		year: {
			values: Array(12)
				.fill(0)
				.map((item) => Math.floor(Math.random() * 100)),
			xLabels: Array(12)
				.fill(0)
				.map((item, index) => index + 1),
			yLabels: Array(7)
				.fill(0)
				.map((item, index) => 0 + index * 100)
				.reverse(),
		},
	},
};

const statisticState = {
	isVertical: false,
	charts: {
		now: {
			values: Array(4)
				.fill(0)
				.map((item) => Math.floor(Math.random() * 100)),
			xLabels: Array(11)
				.fill(0)
				.map((item, index) => index * 10),
			yLabels: ["Visitors", "Subscriber", "Contributor", "Author"],
		},
		today: {
			values: Array(4)
				.fill(0)
				.map((item) => Math.floor(Math.random() * 100)),
			xLabels: Array(11)
				.fill(0)
				.map((item, index) => index * 10),
			yLabels: ["Visitors", "Subscriber", "Contributor", "Author"],
		},
		month: {
			values: Array(4)
				.fill(0)
				.map((item) => Math.floor(Math.random() * 100)),
			xLabels: Array(11)
				.fill(0)
				.map((item, index) => index * 10),
			yLabels: ["Visitors", "Subscriber", "Contributor", "Author"],
		},
	},
};

const siteSpeedState = {
	now: {
		highlightedIndex: 2,
		data: [
			{
				name: "Grade",
				unit: "",
				value: 75,
			},
			{
				name: "Page Size",
				unit: "mb",
				value: 1.9,
			},
			{
				name: "Load Time",
				unit: "mc",
				value: 631,
			},
			{
				name: "Requests",
				unit: "",
				value: 42,
			},
		],
	},
	today: {
		highlightedIndex: 2,
		data: [
			{
				name: "Grade",
				unit: "",
				value: 55,
			},
			{
				name: "Page Size",
				unit: "mb",
				value: 1.8,
			},
			{
				name: "Load Time",
				unit: "mc",
				value: 631,
			},
			{
				name: "Requests",
				unit: "",
				value: 69,
			},
		],
	},
	month: {
		highlightedIndex: 2,
		data: [
			{
				name: "Grade",
				unit: "",
				value: 65,
			},
			{
				name: "Page Size",
				unit: "mb",
				value: 1.7,
			},
			{
				name: "Load Time",
				unit: "mc",
				value: 63,
			},
			{
				name: "Requests",
				unit: "",
				value: 42,
			},
		],
	},
};

cardTabSelects.forEach((cardTabSelect) => {
	cardTabSelect.addEventListener("change", (e) => {
		const selectName = e.target.name;

		console.log(e.target.value);

		if (selectName === "user-stat") {
			setVerticalChart(
				userStatChartDiv,
				userStatState.charts[e.target.value],
				userStatState.isVertical
			)
		} else if (selectName === "statistic") {
			setHorizontalChart(
				statisticChartDiv,
				statisticState.charts[e.target.value]
			)
		} else {
			setSiteSpeedCard(siteSpeedDiv, siteSpeedState[e.target.value])
		}
	});
});

userStatTabs.forEach((userStatTab) => {
	userStatTab.addEventListener("click", (e) =>
		onCardTabClick(userStatTabs, userStatTab, () =>
			setVerticalChart(
				userStatChartDiv,
				userStatState.charts[e.target.dataset.tabName],
				userStatState.isVertical
			)
		)
	);
});

statisticTabs.forEach((statisticTab) => {
	statisticTab.addEventListener("click", (e) =>
		onCardTabClick(statisticTabs, statisticTab, () =>
			setHorizontalChart(
				statisticChartDiv,
				statisticState.charts[e.target.dataset.tabName]
			)
		)
	);
});

siteSpeedTabs.forEach((siteSpeedTab) => {
	siteSpeedTab.addEventListener("click", (e) =>
		onCardTabClick(siteSpeedTabs, siteSpeedTab, () =>
			setSiteSpeedCard(siteSpeedDiv, siteSpeedState[e.target.dataset.tabName])
		)
	);
});

const setVerticalChart = (chartDiv, { xLabels, yLabels, values }) => {
	const xLabelsDiv = chartDiv.querySelector(".chart-x-labels");
	const yLabelsDiv = chartDiv.querySelector(".chart-y-labels");
	const chartValuesDiv = chartDiv.querySelector(".chart-values");

	chartValuesDiv.innerHTML = "";
	yLabelsDiv.innerHTML = "";
	xLabelsDiv.innerHTML = "";

	const maxPossibleValue = yLabels.reduce((prev, acc) => {
		return Math.max(prev, acc);
	}, yLabels[0]);

	values.forEach((chartValue) => {
		const chartValueDiv = document.createElement("div");
		const label = document.createElement("div");

		chartValueDiv.classList.add("chart-value");
		label.classList.add("label", "hidden");
		label.innerHTML = `<div class="label-arrow"></div>${Math.floor(
			maxPossibleValue * (chartValue / 100)
		)} Users`;

		chartValueDiv.style.height = `${chartValue}%`;

		chartValueDiv.addEventListener("click", (e) => {
			chartValuesDiv
				.querySelectorAll(".label")
				.forEach((label) => label.classList.add("hidden"));
			chartValueDiv.querySelector(".label").classList.remove("hidden");
		});

		chartValueDiv.appendChild(label);
		chartValuesDiv.appendChild(chartValueDiv);
	});

	xLabels.forEach((label) => {
		const xLabelDiv = document.createElement("div");
		xLabelDiv.innerText = label;
		xLabelsDiv.appendChild(xLabelDiv);
	});

	yLabels.forEach((label) => {
		const yLabelDiv = document.createElement("div");
		yLabelDiv.innerText = label;
		yLabelsDiv.appendChild(yLabelDiv);
	});
};

const setHorizontalChart = (chartDiv, { xLabels, yLabels, values }) => {
	const xLabelsDiv = chartDiv.querySelector(".chart-x-labels");
	const yLabelsDiv = chartDiv.querySelector(".chart-y-labels");
	const chartValuesDiv = chartDiv.querySelector(".chart-values");

	chartValuesDiv.innerHTML = "";
	yLabelsDiv.innerHTML = "";
	xLabelsDiv.innerHTML = "";

	const maxPossibleValue = xLabels.reduce((prev, acc) => {
		return Math.max(prev, acc);
	}, xLabels[0]);

	values.forEach((chartValue) => {
		const chartValueDiv = document.createElement("div");
		const label = document.createElement("div");

		chartValueDiv.classList.add("chart-value");
		label.classList.add("label", "hidden");

		const randomColor = `rgb(${Math.floor(Math.random() * 255)},${Math.floor(
			Math.random() * 255
		)},${Math.floor(Math.random() * 255)})`;

		label.innerHTML = `<div class="label-arrow"></div>${Math.floor(
			maxPossibleValue * (chartValue / 100)
		)}`;
		label.style.backgroundColor = randomColor;
		label.querySelector(
			".label-arrow"
		).style.borderColor = `${randomColor} transparent`;

		chartValueDiv.style.width = `${chartValue}%`;
		chartValueDiv.style.backgroundColor = randomColor;

		chartValueDiv.addEventListener("click", (e) => {
			chartValuesDiv
				.querySelectorAll(".label")
				.forEach((label) => label.classList.add("hidden"));
			chartValueDiv.querySelector(".label").classList.remove("hidden");
		});

		// const labelLine = document.createElement("div");
		// labelLine.classList.add("label-line");
		// chartValueDiv.appendChild(labelLine);

		chartValueDiv.appendChild(label);
		chartValuesDiv.appendChild(chartValueDiv);
	});

	xLabels.forEach((label) => {
		const xLabelDiv = document.createElement("div");
		xLabelDiv.innerText = label;
		xLabelsDiv.appendChild(xLabelDiv);
	});

	yLabels.forEach((label) => {
		const yLabelDiv = document.createElement("div");
		yLabelDiv.innerText = label;
		yLabelsDiv.appendChild(yLabelDiv);
	});
};

const setSiteSpeedCard = (siteSpeedDiv, state) => {
	const highlightedValue = siteSpeedDiv.querySelector(
		".highlighted .highlighted-value"
	);
	const highlightedUnit = siteSpeedDiv.querySelector(
		".highlighted .highlighted-unit"
	);
	const dataGridDiv = siteSpeedDiv.querySelector(".data-grid");

	highlightedValue.innerHTML = "";
	highlightedUnit.innerHTML = "";
	dataGridDiv.innerHTML = "";

	highlightedUnit.innerText = state.data[state.highlightedIndex].unit;
	highlightedValue.innerText = state.data[state.highlightedIndex].value;

	for (const item of state.data) {
		const dataDiv = document.createElement("div");
		let valueDiv = document.createElement("div");
		let nameDiv = document.createElement("div");

		dataDiv.classList.add("data");

		valueDiv.classList.add("data-value");
		nameDiv.classList.add("data-name");

		valueDiv.innerText = `${item.value} ${item.unit}`;
		nameDiv.innerText = item.name;

		dataDiv.appendChild(valueDiv);
		dataDiv.appendChild(nameDiv);

		dataGridDiv.appendChild(dataDiv);
	}
};

const onCardTabClick = (tabs, currentTab, setTabContent) => {
	tabs.forEach((uST) => uST.classList.remove("active"));
	currentTab.classList.add("active");
	setTabContent();
};

setVerticalChart(
	userStatChartDiv,
	userStatState.charts.month,
	userStatState.isVertical
);
setHorizontalChart(
	statisticChartDiv,
	statisticState.charts.now,
	statisticState.isVertical
);

setSiteSpeedCard(siteSpeedDiv, siteSpeedState.now);
