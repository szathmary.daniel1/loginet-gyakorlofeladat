import data from "../assets/json/data.json";
import { createContext, useState } from "react";
import { addDaysToDate, formatDateForInput } from "../utils/date";

export const FlightSearchContext = createContext();

export function FlightSearchContextProvider({ children }) {
	const flightSearchData = data;
	const [flightSearchFormState, setFlightSearchFormState] = useState({
		travelFrom: "",
		travelTo: "",
		isOneWay: false,
		departureDate: formatDateForInput(addDaysToDate(new Date(), 1)),
		returnDate: formatDateForInput(addDaysToDate(new Date(), 8)),
		accessCode: "",
		travelClass: data.travelClasses[0],
		adults: 1,
		children: 0,
		infants: 0,
	});

	const setFlightSearchFormStateProperty = (propertyToChange, newValue) => {
		setFlightSearchFormState((state) => {
			return {
				...state,
				[propertyToChange]: newValue,
			};
		});
	};

	const api = {
		setTravelTo: (newTravelTo) =>
			setFlightSearchFormStateProperty("travelTo", newTravelTo),

		setTravelFrom: (newTravelFrom) =>
			setFlightSearchFormStateProperty("travelFrom", newTravelFrom),

		setIsOneWay: (newIsOneWay) =>
			setFlightSearchFormStateProperty("isOneWay", newIsOneWay),

		setDepartureDate: (newDepartureDate) =>
			setFlightSearchFormStateProperty("departureDate", newDepartureDate),

		setReturnDate: (newReturnDate) =>
			setFlightSearchFormStateProperty("returnDate", newReturnDate),

		setAccessCode: (newAccessCode) =>
			setFlightSearchFormStateProperty("accessCode", newAccessCode),

		setTravelClass: (newTravelClass) =>
			setFlightSearchFormStateProperty("travelClass", newTravelClass),

		setAdults: (newAdults) =>
			setFlightSearchFormStateProperty("adults", newAdults),

		setChildren: (newChildren) =>
			setFlightSearchFormStateProperty("children", newChildren),

		setInfants: (newInfants) =>
			setFlightSearchFormStateProperty("infants", newInfants),

		resetTravelTo: () => setFlightSearchFormStateProperty("travelTo", ""),

		resetTravelFrom: () => setFlightSearchFormStateProperty("travelFrom", ""),

		resetIsOneWay: () => setFlightSearchFormStateProperty("isOneWay", false),

		resetDepartureDate: () =>
			setFlightSearchFormStateProperty("departureDate", ""),

		resetReturnDate: () => setFlightSearchFormStateProperty("returnDate", ""),

		resetAccessCode: () => setFlightSearchFormStateProperty("accessCode", ""),

		resetTravelClass: () => setFlightSearchFormStateProperty("travelClass", ""),

		resetAdults: () => setFlightSearchFormStateProperty("adults", 0),

		resetChildren: () => setFlightSearchFormStateProperty("children", 0),

		resetInfants: () => setFlightSearchFormStateProperty("infants", 0),

		swapLocations: () => {
			const prevTravelTo = flightSearchFormState.travelTo;
			const prevTravelFrom = flightSearchFormState.travelFrom;
			setFlightSearchFormStateProperty("travelTo", prevTravelFrom);
			setFlightSearchFormStateProperty("travelFrom", prevTravelTo);
		},

		searchCitiesByName: (cityNamePart) => {
			return flightSearchData.cities.filter((city) =>
				city.name.toLowerCase().includes(cityNamePart.toLowerCase())
			);
		},
	};

	return (
		<FlightSearchContext.Provider
			value={{ flightSearchData, flightSearchFormState, ...api }}>
			{children}
		</FlightSearchContext.Provider>
	);
}
