import { useContext, useEffect, useMemo, useState } from "react";
import { FlightSearchContext } from "../contexts/FlightSearchContext";
import FlightSearchStepper from "./FlightSearchStepper";
import FlightSearchDropdown from "./FlightSearchDropdown";
import getObjectValuesAsArray from "../utils/getObjectValuesAsArray";

export default function FlightSearchModal({ isOpen, onClose, onSave }) {
	const { flightSearchFormState, flightSearchData } =
		useContext(FlightSearchContext);

	const [modalState, setModalState] = useState({
		travelClass: flightSearchFormState.travelClass,
		adults: flightSearchFormState.adults,
		children: flightSearchFormState.children,
		infants: flightSearchFormState.infants,
	});

	const [modalStateError, setModalStateError] = useState({
		travelClass: "",
		adults: "",
		children: "",
		infants: "",
	});

	const isModalStateValid = useMemo(() => {
		return getObjectValuesAsArray(modalStateError).every(
			(error) => error === ""
		);
	}, [modalStateError]);

	useEffect(() => {
		document.body.classList.toggle("overflow-y-hidden", isOpen);
	}, [isOpen]);

	useEffect(() => {
		const isTotalPassengerValid =
			modalState.adults + modalState.children + modalState.infants <= 9;

		setModalStateError((state) => {
			return {
				...state,
				adults: isTotalPassengerValid
					? ""
					: "The sum of all passengers must not exceed 9.",
			};
		});
	}, [modalState]);

	const handleCloseModal = (e) => {
		if (
			!e.target.closest(".flight-search-modal") ||
			e.target.classList.contains("close-btn")
		) {
			const { travelClass, adults, children, infants } = flightSearchFormState;
			setModalState({ travelClass, adults, children, infants });
			setModalStateError({
				travelClass: "",
				adults: "",
				children: "",
				infants: "",
			});
			onClose();
		}
	};

	const handleSelectTravelClass = (newTravelClass) => {
		setModalState((state) => {
			return {
				...state,
				travelClass: newTravelClass,
			};
		});
	};

	const handleSaveModal = () => {
		if (isModalStateValid) {
			onSave(modalState);
		}
	};

	const handleAdultsStep = (newAdults) => {
		setModalState((state) => {
			return {
				...state,
				adults: newAdults,
			};
		});

		const isInfantsNumberValid = modalState.infants <= newAdults;

		setModalStateError((state) => {
			return {
				...state,
				infants: isInfantsNumberValid
					? ""
					: "No. of infants must be less or equal adults",
			};
		});
	};

	const handleChildrenStep = (newChildren) => {
		setModalState((state) => {
			return {
				...state,
				children: newChildren,
			};
		});
	};

	const handleInfantsStep = (newInfants) => {
		setModalState((state) => {
			return {
				...state,
				infants: newInfants,
			};
		});

		const isInfantsNumberValid = newInfants <= modalState.adults;

		setModalStateError((state) => {
			return {
				...state,
				infants: isInfantsNumberValid
					? ""
					: "No. of infants must be less or equal adults",
			};
		});
	};

	return (
		<div
			className={"flight-search-modal-wrapper " + (isOpen ? "" : "hidden")}
			onClick={handleCloseModal}>
			<dialog className="flight-search-modal" open={isOpen}>
				<div className="flight-search-modal-header">
					<span>Travel Details</span>
					<button
						className="btn icon-btn close-btn"
						type="button"
						onClick={handleCloseModal}>
						Close X
					</button>
				</div>
				<div className="flight-search-modal-body">
					<FlightSearchDropdown
						label={"Travel class"}
						options={flightSearchData.travelClasses}
						value={modalState.travelClass}
						onSelect={handleSelectTravelClass}></FlightSearchDropdown>
					<div className="steppers">
						<div className="stepper-row">
							<div className="stepper-name">
								Adults
								{modalStateError.adults && (
									<div className="error-message">{modalStateError.adults}</div>
								)}
							</div>
							<FlightSearchStepper
								invalid={!!modalStateError.adults}
								options={flightSearchData.adultsOptions}
								onStep={handleAdultsStep}
								value={modalState.adults}></FlightSearchStepper>
						</div>
						<div className="stepper-row">
							<div className="stepper-name">
								Children <div className="input-label">(2 - 11 years)</div>
								{modalStateError.children && (
									<div className="error-message">
										{modalStateError.children}
									</div>
								)}
							</div>
							<FlightSearchStepper
								invalid={!!modalStateError.children}
								options={flightSearchData.childrenOptions}
								onStep={handleChildrenStep}
								value={modalState.children}></FlightSearchStepper>
						</div>
						<div className="stepper-row">
							<div className="stepper-name">
								Infants<div className="input-label">(0 - 23 months)</div>
								{modalStateError.infants && (
									<div className="error-message">{modalStateError.infants}</div>
								)}
							</div>
							<FlightSearchStepper
								invalid={!!modalStateError.infants}
								options={flightSearchData.infantsOptions}
								onStep={handleInfantsStep}
								value={modalState.infants}></FlightSearchStepper>
						</div>
					</div>
				</div>
				<div className="flight-search-modal-footer">
					<button
						className="btn btn-primary not-allowed"
						type="button"
						disabled={!isModalStateValid}
						onClick={handleSaveModal}>
						Continue
					</button>
				</div>
			</dialog>
		</div>
	);
}
