import { FlightSearchContextProvider } from "../contexts/FlightSearchContext";
// import useGoogleSheet from "../hooks/useGoogleSheets";
import FlightSearch from "./FlightSearch";

function App() {
	return (
		<FlightSearchContextProvider>
			<div className="container">
				<FlightSearch></FlightSearch>
			</div>
		</FlightSearchContextProvider>
	);
}

export default App;
