import classNames from "classnames";
import { useState } from "react";
import FlightSearchForm from "./FlightSearchForm";

export default function FlightSearch() {
	const [isFormExpanded, setIsFormExpanded] = useState(false);

	const handleFormExpand = () => {
		setIsFormExpanded(true);
	};

	const handleFormShrink = () => {
		setIsFormExpanded(false);
	};

	const handleRobeClick = () => {
		setIsFormExpanded(false);
	};

	return (
		<>
			<div
				className={classNames("flight-search-robe", { focus: isFormExpanded })}
				onClick={handleRobeClick}></div>
			<div className="flight-search">
				<div className="flight-search-tab-wrapper">
					<div className="form-tab">
						<div className="form-tab-link active">Flights</div>
						<div className="form-tab-link">Rental cars</div>
						<div className="form-tab-link">Hotel</div>
					</div>
				</div>
				<div className="flight-search-form-wrapper">
					<FlightSearchForm
						onFormExpand={handleFormExpand}
						onFormShrink={handleFormShrink}
						isFormExpanded={isFormExpanded}></FlightSearchForm>
				</div>
			</div>
		</>
	);
}
