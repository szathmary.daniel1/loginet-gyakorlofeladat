import classNames from "classnames";
import { useEffect, useState } from "react";

export default function FlightSearchStepper({
	options,
	value,
	onStep,
	invalid = false,
}) {
	const [stepIndex, setStepIndex] = useState(
		options.findIndex((option) => option === value)
	);
	const [hasNext, setHasNext] = useState(stepIndex < options.length - 1);
	const [hasPrev, setHasPrev] = useState(stepIndex > 0);
	const [isOpen, setIsOpen] = useState(false);

	useEffect(() => {
		const newValueIndex = options.findIndex((option) => option === value);
		const newStepIndex = newValueIndex !== -1 ? newValueIndex : 0;

		setStepIndex(newStepIndex);
		setHasNext(newStepIndex < options.length - 1);
		setHasPrev(newStepIndex > 0);
	}, [options, value]);

	const handleBlur = () => {
		setIsOpen(false);
	};

	const handleMouseDown = (e) => e.preventDefault();

	const handleSelectOption = (newSelectedOption) => {
		const newStepIndex = options.findIndex(
			(option) => option === newSelectedOption
		);

		if (newStepIndex !== -1) {
			onStep(options[newStepIndex]);
		}

		setIsOpen(false);
	};

	const handleToggleDropdown = () => {
		setIsOpen((state) => !state);
	};

	const handlePrevStep = () => {
		const newStepIndex = stepIndex - 1;
		if (newStepIndex >= 0) {
			onStep(options[newStepIndex]);
		}
	};

	const handleNextStep = () => {
		const newStepIndex = stepIndex + 1;
		if (newStepIndex < options.length) {
			onStep(options[newStepIndex]);
		}
	};

	return (
		<div className={classNames("flight-search-stepper", { invalid })}>
			<button
				className="btn stepper-btn"
				type="button"
				onClick={handlePrevStep}
				disabled={!hasPrev}>
				<i>-</i>
			</button>
			<div className="flight-search-dropdown stepper-dropdown">
				<button
					className="btn flight-search-dropdown-btn"
					type="button"
					onClick={handleToggleDropdown}
					onBlur={handleBlur}>
					<span>{options[stepIndex]}</span>
				</button>
				{isOpen && (
					<div className="flight-search-dropdown-options">
						{options.map((option, index) => (
							<div
								key={`option-${index}`}
								className={classNames("option", {
									selected: option === options[stepIndex],
								})}
								onMouseDown={handleMouseDown}
								onClick={() => handleSelectOption(option)}>
								{option}
							</div>
						))}
					</div>
				)}
			</div>
			<button
				className="btn stepper-btn"
				type="button"
				onClick={handleNextStep}
				disabled={!hasNext}>
				<i>+</i>
			</button>
		</div>
	);
}
