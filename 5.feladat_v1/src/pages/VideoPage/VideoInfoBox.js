import { useNavigate } from "react-router-dom";
import "../../assets/sass/videoInfoBox.scss";

export default function VideoInfoBox({ videoInfo }) {
	const navigate = useNavigate();
	const {
		publishedAt,
		channelId,
		title,
		description,
		thumbnail,
		channelTitle,
		viewCount,
		likeCount,
		commentCount,
	} = videoInfo;

	const handleChannelClick = () => {
		navigate(`/channel/${channelId}`, { replace: true });
	};

	return (
		<div className="video-info-box">
			<img
				className="video-info-box-thumbnail"
				src={thumbnail}
				alt={`${title} video thumbnail`}
			/>
			<h2 className="video-info-box-title">{videoInfo.title}</h2>
			<div className="video-info-box-statistics">
				<div>
					<i className="fa-regular fa-clock"></i> {publishedAt}
				</div>
				<div>
					<i className="fa-regular fa-eye"></i> {viewCount}
				</div>
				<div>
					<i className="fa-regular fa-thumbs-up"></i> {likeCount}
				</div>
				<div>
					<i className="fa-regular fa-comment"></i> {commentCount}
				</div>
			</div>
			<button
				className="btn btn-primary btn-lg btn-rounded btn-p-md"
				onClick={handleChannelClick}>
				{channelTitle}
			</button>
			<div className="video-info-box-description">{description}</div>
		</div>
	);
}
