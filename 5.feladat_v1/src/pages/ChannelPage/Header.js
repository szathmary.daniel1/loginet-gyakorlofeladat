import "../../assets/sass/channelHeader.scss";

export default function Header({ channelInfo }) {
	return (
		<div className="channel-header">
			<div className="channel-thumbnail">
				<img src={channelInfo.thumbnail} alt="" />
				<h3 className="channel-title">{channelInfo.title}</h3>
			</div>
			<div className="channel-description">{channelInfo.description}</div>
			<div className="channel-infos">
				<div className="channel-info">
					<h4>Published at: </h4> {channelInfo.publishedAt}
				</div>
				<div className="channel-info">
					<h4>View Count: </h4> {channelInfo.viewCount}
				</div>
				<div className="channel-info">
					<h4>Subscriber count: </h4> {channelInfo.subscriberCount}
				</div>
				<div className="channel-info">
					<h4>Video count: </h4> {channelInfo.videoCount}
				</div>
			</div>
		</div>
	);
}
