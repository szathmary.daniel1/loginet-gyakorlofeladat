import { useState, useEffect } from "react";
import { Link, useNavigate, useParams } from "react-router-dom";
import youtubeApi from "../../api/youtubeApi";
import Header from "./Header";
import VideoGrid from "../../components/VideoGrid";
import PaginationButtons from "../../components/PaginationButtons";

export default function ChannelPage() {
	const { getChannelInfosById, getVideosByChannelId } = youtubeApi;

	const { channelId } = useParams();
	const navigate = useNavigate();

	const [nextPageToken, setNextPageToken] = useState("");
	const [prevPageToken, setPrevPageToken] = useState("");
	const [videos, setVideos] = useState([]);
	const [channelInfo, setChannelInfo] = useState({
		title: "",
		description: "",
		publishedAt: null,
		thumbnail: "",
		viewCount: 0,
		subscriberCount: 0,
		videoCount: 0,
	});

	useEffect(() => {
		const getChannelInfos = async () => {
			const data = await getChannelInfosById(channelId);

			if (data.pageInfo.totalResults === 0)
				return navigate("/", { replace: true });

			const { title, description, publishedAt } = data.items[0].snippet;
			const thumbnail = data.items[0].snippet.thumbnails.default.url;
			const { viewCount, subscriberCount, videoCount } =
				data.items[0].statistics;

			setChannelInfo((state) => {
				return {
					...state,
					title,
					description,
					publishedAt: new Date(publishedAt).toLocaleDateString(),
					thumbnail,
					viewCount: parseInt(viewCount).toLocaleString(),
					subscriberCount: parseInt(subscriberCount).toLocaleString(),
					videoCount: parseInt(videoCount).toLocaleString(),
				};
			});
		};

		getChannelInfos();
	}, [channelId, getChannelInfosById, navigate]);

	/* eslint-disable */
	useEffect(() => {
		const getVideos = async () => {
			const data = await getVideosByChannelId(channelId);

			setNextPageToken(data.nextPageToken ?? "");
			setPrevPageToken("");

			const videos = videoDataMapper(data);
			setVideos(videos);
		};
		getVideos();
	}, []);
	/* eslint-enable */

	const handleNextPageClick = async () => {
		if (nextPageToken === "") return;

		const data = await getVideosByChannelId(channelId, nextPageToken);

		setPrevPageToken(data.prevPageToken ?? "");
		setNextPageToken(data.nextPageToken ?? "");

		const videos = videoDataMapper(data);
		setVideos(videos);
	};

	const handlePrevPageClick = async () => {
		if (prevPageToken === "") return;

		const data = await getVideosByChannelId(channelId, prevPageToken);

		setNextPageToken(data.nextPageToken ?? "");
		setPrevPageToken(data.prevPageToken ?? "");

		const videos = videoDataMapper(data);
		setVideos(videos);
	};

	return (
		<div className="container-page-channel">
			<Link to="/" replace={true}>
				<i className="fa-solid fa-arrow-left"></i> Back to home
			</Link>
			<Header channelInfo={channelInfo} />
			<VideoGrid videos={videos} />
			<PaginationButtons
				onNext={handleNextPageClick}
				onPrev={handlePrevPageClick}
				hasNext={nextPageToken !== ""}
				hasPrev={prevPageToken !== ""}
			/>
		</div>
	);
}

const videoDataMapper = (data) =>
	data.items.map((item) => {
		const id = item.id.videoId;
		const { description, publishedAt, title } = item.snippet;
		const thumbnail = item.snippet.thumbnails.medium.url;

		return {
			id,
			description,
			publishedAt: new Date(publishedAt).toDateString(),
			title,
			thumbnail,
		};
	});
