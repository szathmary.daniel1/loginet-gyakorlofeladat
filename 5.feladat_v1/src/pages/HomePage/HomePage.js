import { useState } from "react";
import { useNavigate } from "react-router-dom";
import youtubeApi from "../../api/youtubeApi";
import PaginationButtons from "../../components/PaginationButtons";
import Searchbar from "../../components/Searchbar";
import ChannelList from "./ChannelList";

export default function HomePage() {
	const { getChannelsByQuery } = youtubeApi;

	const navigate = useNavigate();

	const [nextPageToken, setNextPageToken] = useState("");
	const [prevPageToken, setPrevPageToken] = useState("");
	const [query, setQuery] = useState("");
	const [channels, setChannels] = useState([]);

	const handleSearch = async (query) => {
		setQuery(query);
		if (query === "") {
			setChannels([]);
			return;
		}
		const data = await getChannelsByQuery(query);

		setNextPageToken(data.nextPageToken ?? "");
		setPrevPageToken("");

		const newChannels = channelDataMapper(data);
		setChannels(newChannels);
	};

	const handleChannelItemClick = (channelId) => {
		navigate(`/channel/${channelId}`, { replace: true });
	};

	const handleNextPageClick = async () => {
		if (nextPageToken === "") return;

		const data = await getChannelsByQuery(query, nextPageToken);

		setPrevPageToken(data.prevPageToken ?? "");
		setNextPageToken(data.nextPageToken ?? "");

		const newChannels = channelDataMapper(data);
		setChannels(newChannels);
	};

	const handlePrevPageClick = async () => {
		if (prevPageToken === "") return;

		const data = await getChannelsByQuery(query, prevPageToken);

		setNextPageToken(data.nextPageToken ?? "");
		setPrevPageToken(data.prevPageToken ?? "");

		const newChannels = channelDataMapper(data);
		setChannels(newChannels);
	};

	return (
		<div className="container-page-home">
			<Searchbar onSearch={handleSearch}></Searchbar>
			<ChannelList channels={channels} onClick={handleChannelItemClick} />
			{channels.length > 0 ? (
				<PaginationButtons
					onNext={handleNextPageClick}
					onPrev={handlePrevPageClick}
					hasNext={nextPageToken !== ""}
					hasPrev={prevPageToken !== ""}
				/>
			) : null}
		</div>
	);
}

const channelDataMapper = (data) => data.items.map((item) => item.snippet);
