import ChannelListItem from "./ChannelListItem";
import "../../assets/sass/channelList.scss";

export default function ChannelList({ channels, onClick }) {
	return (
		<div className="channel-list">
			{channels.map((channel) => (
				<ChannelListItem
					key={`channel-list-item-${channel.channelId}`}
					channel={channel}
					onClick={onClick}
				/>
			))}
		</div>
	);
}
