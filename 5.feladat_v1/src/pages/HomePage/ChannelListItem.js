export default function ChannelListItem({ channel, onClick }) {
	const handleClick = () => {
		onClick(channel.channelId);
	};

	return (
		<div className="channel-list-item" onClick={handleClick}>
			<div className="channel-list-item-thumbnail">
				<img src={channel.thumbnails.default.url} alt="channel thumbnail" />
			</div>
			<h3 className="channel-list-item-title">{channel.title}</h3>
			<div className="channel-list-item-description">{channel.description}</div>
		</div>
	);
}
