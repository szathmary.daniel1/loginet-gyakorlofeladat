export default function PaginationButtons({
	onNext,
	onPrev,
	hasNext,
	hasPrev,
}) {
	return (
		<div className="pagination-buttons">
			<button
				className="btn btn-secondary btn-xl"
				disabled={!hasPrev}
				onClick={onPrev}>
				&#xab;
			</button>
			<button
				className="btn btn-secondary btn-xl"
				disabled={!hasNext}
				onClick={onNext}>
				&#xbb;
			</button>
		</div>
	);
}
