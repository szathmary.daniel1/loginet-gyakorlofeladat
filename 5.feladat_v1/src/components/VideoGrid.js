import VideoGridItem from "./VideoGridItem";
import "../assets/sass/videoGrid.scss";
import { useNavigate } from "react-router-dom";

export default function VideoGrid({ videos }) {
	const navigate = useNavigate();

	const handleVideoClick = (videoId) => {
		navigate(`/video/${videoId}`, { replace: true });
	};

	return (
		<div className="video-grid">
			{videos.map((video) => (
				<VideoGridItem
					key={`video-${video.id}`}
					video={video}
					onClick={() => handleVideoClick(video.id)}
				/>
			))}
		</div>
	);
}
