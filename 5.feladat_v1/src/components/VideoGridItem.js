export default function VideoGridItem({ video, onClick }) {
	const { title, publishedAt, thumbnail } = video;

	return (
		<div className="video-grid-item" onClick={onClick}>
			<div className="video-thumbnail">
				<img src={thumbnail} alt={`${title} video thumbnail`} />
			</div>
			<div className="video-title">{title}</div>
			<div className="video-published">
				<i className="fa-regular fa-clock"></i> {publishedAt}
			</div>
		</div>
	);
}
