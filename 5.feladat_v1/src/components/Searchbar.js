import { useRef } from "react";
import "../assets/sass/searchbar.scss";

export default function Searchbar({ onSearch }) {
	const inputRef = useRef();

	const handleSearch = () => {
		onSearch(inputRef.current.value);
	};

	const handleKeyDown = (e) => {
		if (e.key === "Enter") {
			handleSearch();
		}
	};

	return (
		<div className="searchbar">
			<i className="fa-solid fa-magnifying-glass searchbar-icon"></i>
			<input
				type="text"
				className="searchbar-input"
				ref={inputRef}
				placeholder="Search"
				onKeyDown={handleKeyDown}
			/>
			<button className="searchbar-btn" onClick={handleSearch}>
				Search
			</button>
		</div>
	);
}
