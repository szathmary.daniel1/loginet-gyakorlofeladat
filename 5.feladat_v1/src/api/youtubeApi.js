import axios from "axios";

const axiosClient = axios.create({
	baseURL: process.env.REACT_APP_BASE_URL,
});

axiosClient.interceptors.request.use((config) => {
	config.params = config.params || {};
	config.params.key = process.env.REACT_APP_API_KEY;
	return config;
});

const youtubeApi = {
	getChannelsByQuery: async (query, pageToken = "") => {
		return (
			await axiosClient.get("search", {
				params: {
					part: "snippet",
					q: query,
					type: "channel",
					pageToken,
				},
			})
		).data;
	},

	getChannelInfosById: async (channelId, pageToken = "") => {
		return (
			await axiosClient.get("channels", {
				params: {
					part: "snippet,statistics",
					id: channelId,
					pageToken,
				},
			})
		).data;
	},

	getVideosByChannelId: async (channelId, pageToken = "") => {
		return (
			await axiosClient.get("search", {
				params: {
					part: "snippet",
					type: "video",
					maxResults: 30,
					order: "date",
					channelId,
					pageToken,
				},
			})
		).data;
	},

	getVideoInfosById: async (videoId, pageToken = "") => {
		return (
			await axiosClient.get("videos", {
				params: {
					part: "snippet,statistics",
					id: videoId,
					pageToken,
				},
			})
		).data;
	},

	getRelatedToVideoId: async (videoId, pageToken = "") => {
		return (
			await axiosClient.get("search", {
				params: {
					part: "snippet",
					type: "video",
					maxResults: 30,
					order: "date",
					relatedToVideoId: videoId,
					pageToken,
				},
			})
		).data;
	},
};

export default youtubeApi;
