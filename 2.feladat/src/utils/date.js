const oneDayInMilliseconds = 24 * 60 * 60 * 1000;

export const formatDateForInput = (date) => {
    return date.toISOString().slice(0, 10);
}

export const addDaysToDate = (date,daysToAdd) => {
    return new Date(date.getTime() + daysToAdd * oneDayInMilliseconds);
};