import classNames from "classnames";
import { useRef } from "react";

export default function FlightSearchInput({
	type,
	value,
	name,
	placeholder,
	bottomLabel,
	errorMessage,
	invalid = false,
	optional = false,
	onChange,
	onReset,
	onBlur = () => {},
}) {
	const inputRef = useRef();

	const handleChange = (e) => {
		onChange(e.target.value);
	};

	const handleReset = () => {
		onReset();
		inputRef.current.blur();
	};

	const handleBlur = () => {
		onBlur();
	};

	return (
		<div
			className={classNames(
				"flight-search-input-wrapper",
				{ optional },
				{ invalid }
			)}>
			<label htmlFor={name} className="flight-search-input-label">
				{placeholder}
			</label>
			<input
				key={name + "-input"}
				className="flight-search-input"
				type={type || "text"}
				name={name}
				value={value}
				ref={inputRef}
				onChange={handleChange}
				placeholder={placeholder}
				onBlur={handleBlur}
			/>
			{value && (
				<button type="button" className="btn reset-btn" onClick={handleReset}>
					<i className="fa-solid fa-x"></i>
				</button>
			)}
			{optional && <div className="input-label">optional</div>}
			{bottomLabel && <div className="bottom-label">{bottomLabel}</div>}
			{invalid && <div className="error-message">{errorMessage}</div>}
		</div>
	);
}
