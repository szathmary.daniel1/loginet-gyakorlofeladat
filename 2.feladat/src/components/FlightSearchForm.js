import classNames from "classnames";
import { useContext, useEffect, useMemo, useState } from "react";
import { FlightSearchContext } from "../contexts/FlightSearchContext";
import getObjectValuesAsArray from "../utils/getObjectValuesAsArray";
import FlightSearchInput from "./FlightSearchInput";
import FlightSearchLocationInput from "./FlightSearchLocationInput";
import FlightSearchModal from "./FlightSearchModal";

export default function FlightSearchForm({
	isFormExpanded,
	onFormExpand,
	onFormShrink,
}) {
	const [isFlightSearchModalOpen, setIsFlightSearchModalOpen] = useState(false);
	const [canShowErrorBox, setCanShowErrorBox] = useState(false);
	const [hasAccessCode, setHasAccessCode] = useState(false);
	const [formErrors, setFormErrors] = useState({
		travelTo: "",
		travelFrom: "",
		departureDate: "",
		returnDate: "",
	});

	const {
		flightSearchFormState,
		swapLocations,
		setTravelTo,
		setTravelFrom,
		setDepartureDate,
		setReturnDate,
		setIsOneWay,
		setTravelClass,
		setAdults,
		setChildren,
		setInfants,
		setAccessCode,
		resetTravelFrom,
		resetTravelTo,
		resetDepartureDate,
		resetReturnDate,
		resetAccessCode,
	} = useContext(FlightSearchContext);

	const {
		travelFrom,
		travelTo,
		departureDate,
		returnDate,
		isOneWay,
		travelClass,
		adults,
		children,
		infants,
		accessCode,
	} = flightSearchFormState;

	const getNoOfErrors = (formErrors) => {
		return getObjectValuesAsArray(formErrors).reduce((prev, acc) => {
			return prev + (acc !== "" ? 1 : 0);
		}, 0);
	};

	const formatTravellerAndTravelClass = (
		travelClass,
		adults,
		children,
		infants
	) => {
		let result = `${adults} Adults,`;

		result += children === 0 ? "" : ` ${children} Child,`;
		result += infants === 0 ? "" : ` ${infants} Infant,`;
		result += ` ${travelClass}`;

		return result;
	};

	const isFormCorrect = useMemo(
		() => getObjectValuesAsArray(formErrors).every((error) => error === ""),
		[formErrors]
	);

	useEffect(() => {
		setCanShowErrorBox((state) => (isFormCorrect ? false : state));
	}, [isFormCorrect, setCanShowErrorBox]);

	const handleSubmit = (e) => {
		e.preventDefault();

		setCanShowErrorBox(true);

		setFormErrors((state) => {
			return {
				...state,
				departureDate:
					departureDate === "" ? "Please enter your outbound date." : "",
				returnDate:
					returnDate === "" && !isOneWay
						? "Please enter your return date."
						: "",
				travelFrom:
					travelFrom === ""
						? "Please enter your origin and try again."
						: state.travelFrom,
				travelTo:
					travelTo === ""
						? "Please enter your destination and try again."
						: state.travelTo,
			};
		});

		if (!isFormCorrect) return;

		console.log(flightSearchFormState);
	};

	const handleIsOneWayChange = (e) => {
		const isOneWayChecked = e.target.checked;
		const returnDateError = isOneWayChecked ? "" : formErrors.returnDate;

		setIsOneWay(isOneWayChecked);
		setFormErrors((state) => {
			return {
				...state,
				returnDate: returnDateError,
			};
		});
	};

	const handleSwapLocations = () => {
		swapLocations();
	};

	const handleFlightSearchModalClose = () => {
		setIsFlightSearchModalOpen(false);
	};

	const handleFlightSearchModalOpen = () => {
		setIsFlightSearchModalOpen(true);
	};

	const handleFlightSearchModalSave = ({
		travelClass,
		adults,
		children,
		infants,
	}) => {
		setTravelClass(travelClass);
		setAdults(adults);
		setChildren(children);
		setInfants(infants);
		setIsFlightSearchModalOpen(false);
	};

	const handleHasAccessCodeClick = () => {
		setHasAccessCode(true);
	};

	const handleResetDates = () => {
		resetDepartureDate();
		resetReturnDate();
	};

	const handleSetTravelTo = (newTravelTo) => {
		setTravelTo(newTravelTo);

		setFormErrors((state) => {
			return {
				...state,
				travelTo:
					newTravelTo === travelFrom
						? "Origin and destination are the same. Please check your input and try again."
						: "",
			};
		});
	};

	const handleSetTravelFrom = (newTravelFrom) => {
		setTravelFrom(newTravelFrom);

		setFormErrors((state) => {
			return {
				...state,
				travelTo:
					travelTo === newTravelFrom
						? "Origin and destination are the same. Please check your input and try again."
						: "",
			};
		});
	};

	const handleExpandButtonClick = () => {
		isFormExpanded ? onFormShrink() : onFormExpand();
	};

	const handleLocationInputClick = () => {
		if (!isFormExpanded) {
			onFormExpand();
		}
	};

	const handleErrorOccur = (name, errorMsg) => {
		setFormErrors((state) => {
			return {
				...state,
				[name]: errorMsg,
			};
		});
	};

	const handleDepartureDateChange = (newValue) => {
		setDepartureDate(newValue);
		setFormErrors((state) => {
			return {
				...state,
				departureDate: "",
			};
		});
	};

	const handleDepartureDateReset = () => {
		resetDepartureDate();
		setFormErrors((state) => {
			return {
				...state,
				departureDate: "",
			};
		});
	};

	const handleReturnDateChange = (newValue) => {
		setReturnDate(newValue);
		setFormErrors((state) => {
			return {
				...state,
				returnDate: "",
			};
		});
	};

	const handleReturnDateReset = () => {
		resetReturnDate();
		setFormErrors((state) => {
			return {
				...state,
				returnDate: "",
			};
		});
	};

	return (
		<form onSubmit={handleSubmit} className="flight-search-form">
			<div className="form-content-wrapper">
				{isFormExpanded && canShowErrorBox && !isFormCorrect && (
					<div className="row">
						<div className="form-error-wrapper">
							<h3>
								{`${getNoOfErrors(
									formErrors
								)} error has occurred. Please resolve the following issue before you continue:`}
							</h3>
							<ul className="form-errors">
								{getObjectValuesAsArray(formErrors).map((error, index) => {
									return error !== "" ? (
										<li className="form-error" key={"error-" + index}>
											{error}
										</li>
									) : null;
								})}
							</ul>
						</div>
					</div>
				)}
				<div className="row">
					<FlightSearchLocationInput
						placeholder={"From"}
						defaultValue={"Budapest"}
						value={travelFrom}
						name={"travelFrom"}
						displayName={"origin"}
						invalid={!!formErrors.travelFrom}
						errorMessage={formErrors.travelFrom}
						onErrorOccur={handleErrorOccur}
						onSetLocation={handleSetTravelFrom}
						onReset={resetTravelFrom}
						onClick={handleLocationInputClick}></FlightSearchLocationInput>
					<button
						type="button"
						className="btn swap-button"
						onClick={handleSwapLocations}>
						<i className="fa-solid fa-right-left"></i>
					</button>
					<FlightSearchLocationInput
						placeholder={"To"}
						value={travelTo}
						name={"travelTo"}
						displayName={"destination"}
						invalid={!!formErrors.travelTo}
						errorMessage={formErrors.travelTo}
						onErrorOccur={handleErrorOccur}
						onSetLocation={handleSetTravelTo}
						onReset={resetTravelTo}
						onClick={handleLocationInputClick}></FlightSearchLocationInput>
					<button
						type="button"
						id="expand-btn"
						className={classNames("btn", { "btn-primary": !isFormExpanded })}
						onClick={handleExpandButtonClick}>
						{!isFormExpanded ? "Continue" : "Close X"}
					</button>
				</div>
				{isFormExpanded && (
					<>
						<div className="row">
							<label
								htmlFor="oneWay"
								className="one-way-checkbox"
								onChange={handleIsOneWayChange}>
								<input type="checkbox" name="oneWay" /> One-way
							</label>
						</div>
						<div className="row">
							<div className="date-inputs">
								<FlightSearchInput
									type={"date"}
									name={"departureDate"}
									value={departureDate}
									onChange={handleDepartureDateChange}
									onReset={handleDepartureDateReset}
									invalid={!!formErrors.departureDate}
									errorMessage={formErrors.departureDate}></FlightSearchInput>
								{!isOneWay && (
									<FlightSearchInput
										type={"date"}
										name={"returnDate"}
										value={returnDate}
										onChange={handleReturnDateChange}
										onReset={handleReturnDateReset}
										invalid={!!formErrors.returnDate}
										errorMessage={formErrors.returnDate}></FlightSearchInput>
								)}
								<button
									className="btn reset-date-btn"
									type="button"
									onClick={handleResetDates}>
									<i className="fa-solid fa-arrow-rotate-left"></i>
								</button>
							</div>
							<button
								className="btn btn-secondary"
								type="button"
								onClick={(e) => handleFlightSearchModalOpen()}>
								<span className="input-label">Traveller and travel class</span>
								<span>
									{formatTravellerAndTravelClass(
										travelClass,
										adults,
										children,
										infants
									)}
								</span>
								<i className="fa-solid fa-chevron-down"></i>
							</button>
							<button
								className="btn btn-primary"
								id="search-btn"
								onSubmit={handleSubmit}>
								Search flights
							</button>
						</div>
						<div className="row">
							{hasAccessCode ? (
								<div className="access-code">
									<FlightSearchInput
										placeholder={"Access Code"}
										value={accessCode}
										onChange={setAccessCode}
										onReset={resetAccessCode}
										optional={true}
										bottomLabel={
											"This field is for access codes only. If you have a promotional code, please enter it on the payment page."
										}></FlightSearchInput>
								</div>
							) : (
								<button
									className="btn link-btn"
									type="button"
									onClick={handleHasAccessCodeClick}>
									Access Code
								</button>
							)}
						</div>
					</>
				)}
			</div>
			<FlightSearchModal
				isOpen={isFlightSearchModalOpen}
				onClose={handleFlightSearchModalClose}
				onSave={handleFlightSearchModalSave}></FlightSearchModal>
		</form>
	);
}
