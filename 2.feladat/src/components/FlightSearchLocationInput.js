import { useContext, useEffect, useState } from "react";
import { FlightSearchContext } from "../contexts/FlightSearchContext";
import FlightSearchInput from "./FlightSearchInput";
import debounce from "../utils/debounce.js";

export default function FlightSearchLocationInput({
	onReset,
	onSetLocation,
	onClick,
	onErrorOccur,
	placeholder,
	defaultValue,
	value,
	name = "locationInput",
	displayName = "location",
	errorMessage = "",
	optional = false,
	invalid = false,
}) {
	const { searchCitiesByName } = useContext(FlightSearchContext);

	const [inputValue, setInputValue] = useState(defaultValue || "");
	const [cities, setCities] = useState([]);
	const [foundCites, setFoundCities] = useState(0);

	const getCities = debounce((newCity) => {
		const newCities = newCity ? searchCitiesByName(newCity) : [];
		setFoundCities(newCities.length);
		setCities(newCities);
	}, 500);

	useEffect(() => {
		setInputValue(value);
	}, [value]);

	const handleChange = (newValue) => {
		setInputValue(newValue);
		getCities(newValue);
	};

	const handleReset = () => {
		onReset();
		setInputValue("");
		onErrorOccur(name, `Please enter your ${displayName} and try again.`);
	};

	const handleSelectLocation = (location) => {
		setInputValue(location);
		setCities([]);
		onErrorOccur(name, "");
		onSetLocation(location);
	};

	const handleBlur = () => {
		if ((!optional && inputValue === "") || foundCites === 0) {
			onErrorOccur(name, `Please enter your ${displayName} and try again.`);
		}
		setCities([]);
	};

	return (
		<div
			className="flight-search-dropdown flight-search-location-input"
			onClick={onClick}>
			<FlightSearchInput
				placeholder={placeholder}
				value={inputValue}
				name={name}
				displayName={displayName}
				optional={optional}
				invalid={invalid}
				onChange={handleChange}
				onReset={handleReset}
				onBlur={handleBlur}
				errorMessage={errorMessage}
				onErrorOccur={onErrorOccur}></FlightSearchInput>

			{cities.length !== 0 && (
				<div className="flight-search-dropdown-options">
					{cities.map((city, index) => (
						<div
							key={`location-${index}`}
							className="option"
							onMouseDown={(e) => e.preventDefault()}
							onClick={() => handleSelectLocation(city.name)}>
							<div>{city.name}</div>
							<div>{city.short_form}</div>
							<div>{city.country}</div>
						</div>
					))}
				</div>
			)}
		</div>
	);
}
