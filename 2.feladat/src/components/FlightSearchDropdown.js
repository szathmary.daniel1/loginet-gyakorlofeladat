import classNames from "classnames";
import { useState } from "react";

export default function FlightSearchDropdown({
	label,
	options,
	value,
	onSelect,
}) {
	const [isOpen, setIsOpen] = useState(false);

	const handleBlur = () => {
		setIsOpen(false);
	};

	const handleMouseDown = (e) => e.preventDefault();

	const handleSelectOption = (newSelectedOption) => {
		onSelect(newSelectedOption);
		setIsOpen(false);
	};

	const handleToggleDropdown = () => {
		setIsOpen((state) => !state);
	};

	return (
		<div className="flight-search-dropdown travel-class-dropdown">
			<button
				className="btn btn-secondary flight-search-dropdown-btn"
				type="button"
				onClick={handleToggleDropdown}
				onBlur={handleBlur}>
				<span className="input-label">{label}</span>
				<span>{value}</span>
				<i className="fa-solid fa-chevron-down"></i>
			</button>
			{isOpen && (
				<div className="flight-search-dropdown-options">
					{options.map((option, index) => (
						<div
							key={`option-${index}`}
							className={classNames("option", { selected: option === value })}
							onMouseDown={handleMouseDown}
							onClick={() => handleSelectOption(option)}>
							{option}
						</div>
					))}
				</div>
			)}
		</div>
	);
}
