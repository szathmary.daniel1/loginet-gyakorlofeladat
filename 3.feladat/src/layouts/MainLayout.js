import { Outlet } from "react-router-dom";

export default function MainLayout() {
	return (
		<div className="container-main">
			<div className="container-page">
				<Outlet />
			</div>
		</div>
	);
}
