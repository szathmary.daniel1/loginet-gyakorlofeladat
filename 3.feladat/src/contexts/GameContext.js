import { useMemo, useState, createContext } from "react";
import words from "../assets/json/hangman_words.json";

export const GameContext = createContext();

export function GameContextProvider({ children }) {
	const [isGameStarted, setIsGameStarted] = useState(false);
	const [solution, setSolution] = useState([]);
	const [guessedLetters, setGuessedLetters] = useState([]);
	const [usedLetters, setUsedLetters] = useState([]);
	const [maxGuess, setMaxGuess] = useState();

	const isGameWon = useMemo(() => {
		const goodLetters = Array.from(new Set(solution));
		return goodLetters.every((goodLetter) =>
			guessedLetters.includes(goodLetter)
		);
	}, [solution, guessedLetters]);

	const wrongGuesses = useMemo(
		() => usedLetters.length - guessedLetters.length,
		[usedLetters, guessedLetters]
	);

	const isGameOver = useMemo(
		() => wrongGuesses === maxGuess,
		[wrongGuesses, maxGuess]
	);

	const maxWordLength = words.reduce(
		(prev, acc) => Math.max(prev, acc.length),
		words[0].length
	);
	const minWordLength = words.reduce(
		(prev, acc) => Math.min(prev, acc.length),
		words[0].length
	);

	const hasSavedGame = localStorage.getItem("gameSave") !== null;

	const getRandomWord = (letterLength = minWordLength) => {
		let wordsWithTheGivenLength = words.filter(
			(word) => word.length === letterLength
		);

		if (wordsWithTheGivenLength.length === 0) {
			wordsWithTheGivenLength = words.filter(
				(word) => word.length === minWordLength
			);
		}

		const randomIndex = Math.floor(
			Math.random() * wordsWithTheGivenLength.length
		);

		return [...wordsWithTheGivenLength[randomIndex].toLocaleLowerCase()];
	};

	const api = {
		startGame: (letterLength, maxGuess) => {
			setSolution(getRandomWord(letterLength));
			setMaxGuess(maxGuess);
			setIsGameStarted(true);
		},

		newGame: () => {
			setUsedLetters([]);
			setGuessedLetters([]);
			setSolution([]);
			setMaxGuess(0);
			setIsGameStarted(false);
		},

		saveGame: () => {
			if (isGameOver || isGameWon) return;

			const gameSave = JSON.stringify({
				solution,
				usedLetters,
				guessedLetters,
				maxGuess,
			});
			localStorage.setItem("gameSave", gameSave);
		},

		loadGame: () => {
			const gameSave = JSON.parse(localStorage.getItem("gameSave"));

			setSolution(gameSave.solution);
			setUsedLetters(gameSave.usedLetters);
			setGuessedLetters(gameSave.guessedLetters);
			setMaxGuess(gameSave.maxGuess);
			setIsGameStarted(true);
		},

		deleteSavedGame: () => {
			localStorage.removeItem("gameSave");
		},

		guess: (letter) => {
			if (isGameOver || isGameWon) return;

			if (letter.length !== 1) return;

			letter = letter.toLowerCase();

			if (usedLetters.includes(letter)) return;

			setUsedLetters((state) => [...state, letter]);

			if (!solution.includes(letter)) return;

			setGuessedLetters((state) => [...state, letter]);
		},
	};

	return (
		<GameContext.Provider
			value={{
				isGameStarted,
				isGameOver,
				isGameWon,
				hasSavedGame,
				solution,
				usedLetters,
				guessedLetters,
				wrongGuesses,
				maxWordLength,
				minWordLength,
				...api,
			}}>
			{children}
		</GameContext.Provider>
	);
}
