import classNames from "classnames";
import { useMemo, useContext, useState } from "react";
import { useNavigate } from "react-router-dom";
import { GameContext } from "../contexts/GameContext";
import { hangmanNumberOfPieces } from "../components/Hangman";
import GameMessageModal from "../components/GameMessageModal";

const RANDOM_OPTION = -1;

export default function StartPage() {
	const navigate = useNavigate();

	const {
		startGame,
		hasSavedGame,
		loadGame,
		deleteSavedGame,
		maxWordLength,
		minWordLength,
	} = useContext(GameContext);

	const [isLoadModalOpen, setIsLoadModalOpen] = useState(hasSavedGame);
	const [isDeleteSavedGameModalOpen, setIsDeleteSavedGameModalOpen] =
		useState(false);
	const [letterLength, setLetterLength] = useState(4);
	const [selectedLetterLengthButton, setSelectedLetterLengthButton] =
		useState("4");

	const wordLengths = useMemo(
		() =>
			Array(maxWordLength - minWordLength + 1)
				.fill(1)
				.map((_, index) => minWordLength + index),
		[minWordLength, maxWordLength]
	);

	const handleSelectLetterLength = (e) => {
		if (e.target.innerText !== "Random") {
			setLetterLength(parseInt(e.target.innerText));
			setSelectedLetterLengthButton(parseInt(e.target.innerText));
		} else {
			const newLetterLength =
				Math.floor(Math.random() * (maxWordLength - minWordLength + 1)) +
				minWordLength;
			setLetterLength(newLetterLength);
			setSelectedLetterLengthButton(RANDOM_OPTION);
		}
	};

	const handleStart = () => {
		startGame(letterLength, hangmanNumberOfPieces);
		navigate("/game", {
			replace: true,
		});
	};

	const handleLoadGame = (canLoadGame) => {
		setIsLoadModalOpen(false);

		if (canLoadGame) {
			loadGame();
			navigate("/game", {
				replace: true,
			});
		} else {
			setIsDeleteSavedGameModalOpen(true);
		}
	};

	const handleDeleteSavedGame = (canDeleteSavedGame) => {
		setIsDeleteSavedGameModalOpen(false);

		if (canDeleteSavedGame) {
			deleteSavedGame();
		}
	};

	return (
		<>
			<div className="content-wrapper" id="start">
				<h2>The Hangman</h2>
				<p>
					Let's play <strong>Hangman!</strong> <br />
					How many letters do you want in your word?
				</p>
				<div className="word-options-grid">
					{wordLengths.map((length, index) => (
						<button
							className={classNames("btn btn-secondary btn-md btn-p-md", {
								selected: length === selectedLetterLengthButton,
							})}
							key={`word-length-button-${index}`}
							onClick={handleSelectLetterLength}>
							{length}
						</button>
					))}
					<button
						className={classNames("btn btn-secondary btn-md btn-p-md", {
							selected: RANDOM_OPTION === selectedLetterLengthButton,
						})}
						onClick={handleSelectLetterLength}>
						Random
					</button>
				</div>
				<button
					className="btn btn-primary btn-rounded btn-p-xl btn-sm"
					onClick={handleStart}>
					Let's Play!
				</button>
			</div>
			<GameMessageModal
				isOpen={isLoadModalOpen}
				message={"Do you want to continue your previous game?"}
				onConfirm={() => handleLoadGame(true)}
				onCancel={() => handleLoadGame(false)}
			/>
			<GameMessageModal
				isOpen={isDeleteSavedGameModalOpen}
				message={"Do you want to delete it?"}
				onConfirm={() => handleDeleteSavedGame(true)}
				onCancel={() => handleDeleteSavedGame(false)}
			/>
		</>
	);
}
