import { useContext } from "react";
import { useNavigate } from "react-router-dom";
import { GameContext } from "../contexts/GameContext";
import Hangman from "../components/Hangman";

export default function HomePage() {
	const navigate = useNavigate();
	const { isGameStarted } = useContext(GameContext);

	const handleConfirmButtonClick = () => {
		if (isGameStarted) {
			navigate("/game", {
				replace: true,
			});
		} else {
			navigate("/start", {
				replace: true,
			});
		}
	};

	return (
		<>
			<div className="content-wrapper" id="home">
				<h2>The Hangman</h2>
				<Hangman></Hangman>
				<div>
					<h3>Game instructions</h3>
					<p>
						Lorem ipsum dolor sit amet consectetur adipisicing elit. Veniam
						necessitatibus fugit fuga ducimus sapiente! Asperiores quam esse
						ducimus quae earum incidunt corporis nobis, nisi alias
						exercitationem sint ab in libero!
					</p>
				</div>
				<button
					className="btn btn-primary btn-rounded btn-sm btn-p-xl"
					onClick={handleConfirmButtonClick}>
					Got it!
				</button>
			</div>
		</>
	);
}
