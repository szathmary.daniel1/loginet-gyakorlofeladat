import { useContext } from "react";
import { GameContext } from "../../contexts/GameContext";
import alphabet from "../../constants/alphabet";

export default function AlphabetGrid() {
	const { guess, usedLetters } = useContext(GameContext);

	const isLetterDisabled = (letter) => {
		letter = letter.toLowerCase();
		return usedLetters.includes(letter);
	};

	const handleLetterClick = (letter) => {
		guess(letter);
	};

	return (
		<div className="alphabet-grid-wrapper">
			<div className="label">Play with a word</div>
			<div className="alphabet-grid">
				{alphabet.map((letter, index) => (
					<button
						key={`btn-letter-${index}`}
						className="letter btn btn-secondary btn-lg btn-p-md"
						disabled={isLetterDisabled(letter)}
						onClick={() => handleLetterClick(letter)}>
						{letter}
					</button>
				))}
			</div>
		</div>
	);
}
