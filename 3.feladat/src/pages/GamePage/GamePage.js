import { useState, useContext } from "react";
import { Link, useNavigate } from "react-router-dom";
import { GameContext } from "../../contexts/GameContext";
import AlphabetGrid from "./AlphabetGrid";
import GameMessageModal from "../../components/GameMessageModal";
import Hangman from "../../components/Hangman";
import SolutionLetters from "./SolutionLetters";

export default function GamePage() {
	const navigate = useNavigate();

	const { wrongGuesses, newGame, saveGame, isGameWon, isGameOver } =
		useContext(GameContext);

	const [isSaveModalOpen, setIsSaveModalOpen] = useState(false);

	const handleNewGame = () => {
		newGame();
		navigate("/start", { replace: true });
	};

	const handleSaveGame = (canSaveGame) => {
		setIsSaveModalOpen(false);
		if (canSaveGame) {
			saveGame();
		}
		newGame();
	};

	const handleEndGame = () => {
		setIsSaveModalOpen(true);
	};

	return (
		<>
			<div className="content-wrapper" id="game">
				<Link className="nav-link" to="/" replace={true}>
					Instructions <i className="fa-solid fa-arrow-right-long"></i>
				</Link>
				<Hangman piece={wrongGuesses}></Hangman>
				<h2>The Hangman</h2>
				<SolutionLetters />
				<AlphabetGrid></AlphabetGrid>
				<div className="game-control-buttons">
					<button
						className="btn btn-secondary btn-sm btn-p-md"
						disabled={isGameOver || isGameWon}
						onClick={handleEndGame}>
						END GAME
					</button>
					<button
						className="btn btn-primary btn-sm btn-p-md"
						onClick={handleNewGame}>
						START NEW GAME
					</button>
				</div>
			</div>
			<GameMessageModal
				isOpen={isSaveModalOpen}
				message={"Do you want to save your game?"}
				onConfirm={() => handleSaveGame(true)}
				onCancel={() => handleSaveGame(false)}
			/>
		</>
	);
}
