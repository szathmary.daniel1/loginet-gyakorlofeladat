import classNames from "classnames";
import { useContext } from "react";
import { GameContext } from "../../contexts/GameContext";

export default function SolutionLetters() {
	const { solution, guessedLetters, isGameWon, isGameOver } =
		useContext(GameContext);

	return (
		<div className="solution-letters-wrapper">
			{isGameWon ? (
				<div className="label result won">
					<i className="fa-regular fa-face-smile"></i> You've won
				</div>
			) : isGameOver ? (
				<div className="label result lost">
					<i className="fa-regular fa-face-frown"></i> You've lost
				</div>
			) : (
				<div className={classNames("label")}>
					It's a {solution.length} letter word
				</div>
			)}
			<div className="solution-letters">
				{solution.map((letter, index) => {
					const isLetterGuessed = guessedLetters.includes(letter);

					return (
						<div
							className={classNames("letter", { show: isLetterGuessed })}
							key={`letter-${letter}-${index}`}>
							{isLetterGuessed ? letter : "#"}
						</div>
					);
				})}
			</div>
		</div>
	);
}
