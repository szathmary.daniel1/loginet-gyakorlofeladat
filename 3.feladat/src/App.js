import { RouterProvider, createBrowserRouter } from "react-router-dom";
import { useContext } from "react";
import { GameContext } from "./contexts/GameContext";
import ProtectedRoute from "./components/ProtectedRoute";
import MainLayout from "./layouts/MainLayout";
import HomePage from "./pages/HomePage";
import StartPage from "./pages/StartPage";
import GamePage from "./pages/GamePage/GamePage";

function App() {
	const { isGameStarted } = useContext(GameContext);
	const router = createBrowserRouter([
		{
			path: "/",
			element: <MainLayout />,
			children: [
				{
					index: true,
					element: <HomePage />,
				},
				{
					path: "start",
					element: <StartPage />,
				},
				{
					path: "game",
					element: (
						<ProtectedRoute protect={() => isGameStarted}>
							<GamePage />
						</ProtectedRoute>
					),
				},
			],
		},
	]);

	return <RouterProvider router={router} />;
}

export default App;
