import { Navigate } from "react-router-dom";

export default function ProtectedRoute({ protect, children }) {
	return protect() ? children : <Navigate to="/" replace={true} />;
}
