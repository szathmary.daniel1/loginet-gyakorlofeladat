import Modal from "./Modal";

export default function GameMessageModal({
	isOpen,
	message,
	onConfirm,
	onCancel,
}) {
	return (
		<Modal isOpen={isOpen}>
			<div className="game-message-modal">
				<h3>{message}</h3>
				<button
					className="btn btn-primary btn-sm btn-p-xl btn-rounded"
					onClick={onConfirm}>
					Yes
				</button>
				<button
					className="btn btn-secondary btn-sm btn-p-xl btn-rounded"
					onClick={onCancel}>
					No
				</button>
			</div>
		</Modal>
	);
}
