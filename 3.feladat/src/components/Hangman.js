import React from "react";

const hangmanPieces = [
	<path d="M1,11 h8" />,
	<path d="M9,11 v-10" />,
	<path d="M9,1 h-4" />,
	<path d="M5,1 v2" />,
	<circle cx="5" cy="4" r="1" />,
	<path d="M5,5 v3" />,
	<path d="M5,5 l-2,2" />,
	<path d="M5,5 l2,2" />,
	<path d="M5,8 l-2,2" />,
	<path d="M5,8 l2,2" />,
];

export const hangmanNumberOfPieces = hangmanPieces.length;

export default function Hangman({ piece = hangmanNumberOfPieces }) {
	piece =
		piece < 0
			? 0
			: piece > hangmanNumberOfPieces
			? hangmanNumberOfPieces
			: piece;

	return (
		<svg id="hangman" viewBox="0 0 10 12">
			{hangmanPieces.slice(0, piece).map((element, index) => (
				<React.Fragment key={`hangman-${index}`}>{element}</React.Fragment>
			))}
		</svg>
	);
}
