import { createContext } from "react";
import { useMediaQuery } from "react-responsive";

export const MediaQueryContext = createContext();

export default function MediaQueryContextProvider({ children }) {
	const isDesktop = useMediaQuery({ minWidth: 1080 });
	const isLaptop = useMediaQuery({ minWidth: 720, maxWidth: 1080 });
	const isTablet = useMediaQuery({ maxWidth: 720, minWidth: 480 });
	const isMobile = useMediaQuery({ maxWidth: 480 });

	return (
		<MediaQueryContext.Provider
			value={{ isDesktop, isLaptop, isTablet, isMobile }}>
			{children}
		</MediaQueryContext.Provider>
	);
}
