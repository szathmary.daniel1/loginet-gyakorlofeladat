import ReactDOM from "react-dom/client";
import App from "./components/App";
import "./assets/images/common/quotes.svg";
import "./assets/fonts/gt-america/GT-America-Standard-Bold.woff2";
import "./assets/fonts/gt-america/GT-America-Standard-Medium.woff2";
import "./assets/fonts/gt-america/GT-America-Standard-Regular.woff2";
import "./assets/sass/index.scss";

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(<App />);
