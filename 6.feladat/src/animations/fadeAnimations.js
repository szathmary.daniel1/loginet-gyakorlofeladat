export const fadeInUp = {
	fadeInUpInitial: { y: "20%", opacity: 0 },
	fadeInUp: (delay) => ({
		y: 0,
		x: 0,
		opacity: 1,
		transition: {
			ease: [0.25, 0.1, 0.26, 1.01],
			duration: 0.6,
			delay
		},
	}),
};

export const fadeInDown = {
	fadeInDownInitial: { y: "-20%", opacity: 0 },
    fadeInDownTransitionInitial: { y: "-100%"},
	fadeInDown: (delay) => ({
		y: 0,
		x: 0,
		opacity: 1,
		transition: {
			ease: [0.25, 0.1, 0.26, 1.01],
			duration: 0.6,
			delay,
		},
	}),
    fadeInDownTransition: (delay) => ({
		y: "0%",
		transition: {
			ease: [0.25, 0.1, 0.26, 1.01],
			duration: 0.6,
			delay,
		},
	})
};

export const fadeInLeft = {
	fadeInLeftInitial: { x: "20%", opacity: 0 },
	fadeInLeft: (delay) => ({
		x: "0%",
		opacity: 1,
		transition: {
			ease: [0.25, 0.1, 0.26, 1.01],
			duration: 0.6,
			delay
		},
	}),
};

export const fadeIn = {
	fadeInInitial: { opacity: 0 },
	fadeIn: (delay) => ({
		opacity: 1,
		transition: {
			ease: [0.25, 0.1, 0.26, 1.01],
			duration: 0.6,
			delay
		},
	}),
}