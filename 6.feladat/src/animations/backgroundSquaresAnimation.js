export const backgroundSquaresAnimationVariant = {
    rotate: ({deg,delay}) => ({
        rotate: 360+deg,
        opacity: 1,
        transition: {
            duration: 1,
            ease: [0.25, 0.1, 0.29, 0.93],
            delay
        }
    }),
    rotationInitial: ({deg}) => ({
        rotate: deg,
        opacity: 0
    })
}