export const slideLeft = {
	slideLeft: (duration) => ({
		x: "-750%",
		transition: {
			duration,
			repeat: Infinity,
			repeatType: "mirror"
		},
	}),
	slideLeftQuarter: (duration) => ({
		x: "-187.5%",
		transition: {
			duration,
			repeat: Infinity,
			repeatType: "mirror"
		},
	}),
	slideLeftInitial: {
		x: "0%",
	},
};

export const slideRight = {
	slideRight: (duration) => ({
		x: "750%",
		transition: {
			duration,
			repeat: Infinity,
			repeatType: "mirror"
		},
	}),
	slideRightQuarter: (duration) => ({
		x: "187.5%",
		transition: {
			duration,
			repeat: Infinity,
			repeatType: "mirror"
		},
	}),
	slideRightInitial: {
		x: "0%",
	},
};

export const slideDown = {
	slideDownInitial: { y: "-100%" },
	slideDown: (delay) => ({
		y: 0,
		transition: {
			ease: [0.25, 0.1, 0.26, 1.01],
			duration: 0.6,
			delay,
		},
	}),
};
