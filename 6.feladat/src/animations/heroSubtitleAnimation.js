const infiniteScrollFrames = Array(20)
	.fill(5)
	.map((value, index) => [
		`${parseInt(-1 * (value * index))}%`,
		`${parseInt(-1 * (value * index))}%`,
	])
	.flat();
infiniteScrollFrames.push("-95%");

export const heroSubtitleAnimation = {
	subtitleAnimation: {
		y: infiniteScrollFrames,
		transition: {
			duration: 32,
			ease: "easeInOut",
			repeat: Infinity,
			repeatType: "loop",
		},
	},
	subtitleTransition: {
		duration: 0.6,
		ease: [0.25, 0.1, 0.26, 1.01],
	},
};