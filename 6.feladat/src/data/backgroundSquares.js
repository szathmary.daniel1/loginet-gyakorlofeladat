const backgroundSquares = [
	{
		width: "224px",
		height: "224px",
		fill: "rgba(50,243,150,0.1)",
		deg: 28,
		delay: 0.45,
	},
	{
		width: "40px",
		height: "40px",
		fill: "rgba(50,243,150,0.44)",
		deg: -25,
		delay: 0.6,
	},
	{
		width: "152px",
		height: "152px",
		fill: "rgba(223,248,235,0.77)",
		deg: -25,
		delay: 0.75,
	},
	{
		width: "40px",
		height: "40px",
		fill: "rgba(50,243,150,0.6)",
		deg: -25,
		delay: 0.9,
	},
	{
		width: "224px",
		height: "224px",
		fill: "rgba(47,229,200,0.12)",
		deg: -25,
		delay: 1.05,
	},
	{
		width: "100px",
		height: "100px",
		fill: "rgba(137,137,137,0.2)",
		deg: -25,
		delay: 1.2,
	},
	{
		width: "170px",
		height: "170px",
		fill: "#c3f6a2",
		deg: -25,
		delay: 1.35,
	},
	{
		width: "83px",
		height: "83px",
		fill: "rgba(137,137,137,0.2)",
		deg: -25,
		delay: 1.5,
	},
	{
		width: "248px",
		height: "248px",
		fill: "rgba(195,246,162,0.3)",
		deg: -25,
		delay: 1.65,
	},
];

export default backgroundSquares;
