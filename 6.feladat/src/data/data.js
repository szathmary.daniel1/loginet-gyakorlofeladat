import {
	userProfiles,
	featureIcons,
	featureSlideImages,
	featureImages,
} from "../assets/images";

const data = {
	stories: [
		{
			name: "Ismael",
			profilePicture: userProfiles[0],
			story:
				"Grasshopper showed me that no matter what or who or how I look, anyone can learn how to code. It's exciting. It opens up a whole new world for me.",
		},
		{
			name: "Brianna",
			profilePicture: userProfiles[1],
			story:
				"I like Grasshopper because it allows anyone with or without experience in coding to jump right in and have fun along the way.",
		},
		{
			name: "Zhamal",
			profilePicture: userProfiles[2],
			story:
				"Grasshopper helped me achieve basic understanding of coding. Now, I know that I can see myself in this field.",
		},
		{
			name: "Alverson",
			profilePicture: userProfiles[3],
			story:
				"This is the most user-friendly interactive app [where] challenges are structured like fill-in-the-blanks puzzles [and] animations and confetti ... demonstrate how code can bring things to life.",
		},
	],

	heroSubtitleOptions: [
		"opens new doors",
		"creates new hobbies",
		"launches new careers",
		"develops new skills",
		"expands your network",
	],

	features: [
		{
			icon: featureIcons.visualPuzzles,
			slideImage: featureSlideImages[0],
			image: featureImages[0],
			title:
				"Visual puzzles develop your problem-solving skills and solidify coding concepts.",
		},
		{
			icon: featureIcons.laptopTablet,
			slideImage: featureSlideImages[1],
			image: featureImages[1],
			title:
				"Put your learning into practice with full projects on your laptop or tablet.",
		},
		{
			icon: featureIcons.industryStandard,
			slideImage: featureSlideImages[2],
			image: featureImages[2],
			title:
				"Use industry-standard JavaScript with just a few taps on your phone.",
		},
		{
			icon: featureIcons.feedback,
			slideImage: featureSlideImages[3],
			image: featureImages[3],
			title: " Real-time feedback guides you like a teacher.",
		},
		{
			icon: featureIcons.achievements,
			slideImage: featureSlideImages[4],
			image: featureImages[4],
			title: "Collect achievements as you learn new skills.",
		},
	],

	concepts: [
		"Control Flow",
		"Functions",
		"Variables",
		"Object Editing",
		"Animation",
		"Callbacks",
		"Array Methods",
		"Operators",
		"Recursion",
		"String Manipulation",
		"Loops",
		"Execution Order",
		"Function Declaration",
		"If Statements",
		"Data Structures",
		"Variable Scope",
		"For...Of Loops",
		"and more...",
		"Callbacks",
		"Array Methods",
		"Operators",
		"Recursion",
		"String Manipulation",
		"Loops",
		"Execution Order",
		"Function Declaration",
		"If Statements",
		"Data Structures",
		"Variable Scope",
		"For...Of Loops",
		"Control Flow",
		"Functions",
		"Variables",
		"Object Editing",
		"Animation",
		"Loops",
		"Execution Order",
		"Function Declaration",
		"If Statements",
		"Data Structures",
		"Variable Scope",
		"For...Of Loops",
		"Control Flow",
		"Functions",
		"Variables",
		"Object Editing",
		"Animation",
		"Callbacks",
		"Array Methods",
		"Operators",
		"Recursion",
		"String Manipulation",
	],
};

export default data;
