import { useState } from "react";
import hamburgerMenuIcon from "../assets/images/common/hamburger_menu_icon.png";
import mobileLogo from "../assets/images/common/gh_icon.svg";

export default function NavbarMobile({ type, logo, ...props }) {
	const [isMenuOpen, setIsMenuOpen] = useState(false);

	const handleMenuOpen = () => {
		setIsMenuOpen(true);
	};

	const handleMenuClose = () => {
		setIsMenuOpen(false);
	};

	return (
		<div className="navbar-wrapper" {...props}>
			<nav className={`navbar navbar-mobile navbar-${type}`}>
				<div className="navbar-icon">
					<img src={logo} alt="logo" />
				</div>
				<ul className="navbar-items navbar-items__right">
					<button
						className="btn btn-secondary btn-hamburger"
						onClick={handleMenuOpen}>
						<img src={hamburgerMenuIcon} alt="" />
					</button>
				</ul>
				{isMenuOpen && (
					<nav className="navbar-mobile-menu">
						<div className="navbar-mobile-menu-container">
							<div className="navbar-mobile-menu-logo">
								<img src={mobileLogo} alt="" />
							</div>
							<button
								className="btn btn-secondary btn-close"
								onClick={handleMenuClose}>
								&#10005;
							</button>
							<div className="navbar-mobile-menu-items">
								<div className="navbar-mobile-menu-items-item">
									What is coding
								</div>
								<div className="navbar-mobile-menu-items-item">Curriculum</div>
								<div className="navbar-mobile-menu-items-item">Glossary</div>
								<div className="navbar-mobile-menu-items-item">About us</div>
								<div className="navbar-mobile-menu-items-item">FAQ</div>
								<div className="navbar-mobile-menu-items-item">
									<button className="btn btn-primary btn-rounded btn-xl">
										Get the App
									</button>
								</div>
							</div>
						</div>
					</nav>
				)}
			</nav>
		</div>
	);
}
