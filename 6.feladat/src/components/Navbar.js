import "../assets/sass/navbar.scss";

export default function Navbar({ type, logo, ...props }) {
	return (
		<div className="navbar-wrapper" {...props}>
			<nav className={`navbar navbar-${type}`}>
				<div className="navbar-icon navbar-icon-desktop">
					<img src={logo} alt="logo" />
				</div>
				<ul className="navbar-items navbar-items__left">
					<li className="navbar-items-item">What is Coding</li>
					<li className="navbar-items-item">Curriculum</li>
					<li className="navbar-items-item">Glossary</li>
					<li className="navbar-items-item">About Us</li>
					<li className="navbar-items-item">FAQ</li>
				</ul>
				<ul className="navbar-items navbar-items__right">
					<li className="nav-items-item">Sign In</li>
				</ul>
			</nav>
		</div>
	);
}
