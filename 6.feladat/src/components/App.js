import { motion } from "framer-motion";
import MediaQueryContextProvider from "../contexts/MediaQueryContext";
import StoriesSection from "./sections/StoriesSection";
import ToutsSection from "./sections/ToutsSection";
import LearnToCodeSection from "./sections/LearnToCodeSection";
import HeroSection from "./sections/HeroSection";
import FeatureSection from "./sections/FeatureSection";
import ConceptsSection from "./sections/ConceptsSection";
import GetStartedSection from "./sections/GetStartedSection";
import Footer from "./Footer";
import Header from "./Header";
import { fadeInDown } from "../animations/fadeAnimations";
import data from "../data/data";

export default function App() {
	return (
		<MediaQueryContextProvider>
			<div className="banner-wrapper">
				<motion.div
					variants={fadeInDown}
					initial="fadeInDownTransitionInitial"
					whileInView="fadeInDownTransition"
					custom={0.15}
					viewport={{ once: true }}
					className="banner">
					Now available in Spanish!
				</motion.div>
			</div>
			<Header />
			<main>
				<HeroSection subtitleOptions={data.heroSubtitleOptions} />
				<ToutsSection />
				<StoriesSection stories={data.stories} />
				<LearnToCodeSection />
				<FeatureSection features={data.features} />
				<ConceptsSection concepts={data.concepts} />
				<GetStartedSection />
			</main>
			<Footer />
		</MediaQueryContextProvider>
	);
}
