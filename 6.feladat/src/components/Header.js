import { useContext, useRef } from "react";
import { motion, useInView } from "framer-motion";
import { MediaQueryContext } from "../contexts/MediaQueryContext";
import Navbar from "./Navbar";
import NavbarMobile from "./NavbarMobile";
import { slideDown } from "../animations/slideAnimations";
import logoScroll from "../assets/images/common/gh_icon.svg";
import logo from "../assets/images/common/logo.blue.svg";

export default function Header() {
	const { isDesktop } = useContext(MediaQueryContext);

	const navbarRef = useRef();

	const isNavBarInView = useInView(navbarRef);

	return (
		<motion.header ref={navbarRef}>
			{!isDesktop ? (
				<NavbarMobile logo={logo} type={"light"} />
			) : (
				<Navbar logo={logo} type={"light"}></Navbar>
			)}
			{isDesktop && (
				<motion.div
					variants={slideDown}
					initial="slideDownInitial"
					animate={!isNavBarInView ? "slideDown" : ""}
					custom={0.15}
					className="navbar-scroll-wrapper">
					<Navbar logo={logoScroll} type={"dark"}></Navbar>
				</motion.div>
			)}
		</motion.header>
	);
}
