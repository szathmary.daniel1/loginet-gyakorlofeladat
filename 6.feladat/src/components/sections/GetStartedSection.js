import { useContext, useRef } from "react";
import { motion, useInView } from "framer-motion";
import { MediaQueryContext } from "../../contexts/MediaQueryContext";
import { fadeIn, fadeInUp } from "../../animations/fadeAnimations";
import { slideLeft, slideRight } from "../../animations/slideAnimations";
import grasshopperMama from "../../assets/images/section-get-started/section--get-started__grasshopper-mama.svg";
import cloud from "../../assets/images/section-get-started/section--get-started__cloud.svg";
import googlePlayButton from "../../assets/images/common/button--google-play.png";
import "../../assets/sass/getStarted.scss";

export default function GetStartedSection() {
	const { isLaptop, isDesktop } = useContext(MediaQueryContext);

	const getStartedSectionRef = useRef();

	const wasGetStartedSectionInView = useInView(getStartedSectionRef, {
		once: true,
	});

	const isSmallerThanLaptop = !isLaptop && !isDesktop;

	return (
		<motion.section
			className="get-started"
			variants={fadeIn}
			initial="fadeInInitial"
			animate={wasGetStartedSectionInView ? "fadeIn" : ""}
			custom={0.15}
			ref={getStartedSectionRef}>
			<div className="get-started-background">
				<div className="get-started-background-container">
					<motion.div
						variants={fadeInUp}
						initial="fadeInUpInitial"
						animate={wasGetStartedSectionInView ? "fadeInUp" : ""}
						custom={0.75}
						className="get-started-background-image get-started-background-image-grasshopper-mama">
						<img src={grasshopperMama} alt="" />
					</motion.div>
					<motion.div
						variants={slideRight}
						initial="slideRightInitial"
						animate={wasGetStartedSectionInView ? "slideRight" : ""}
						custom={60}
						className="get-started-background-image get-started-background-image-cloud-1">
						<img src={cloud} alt="" />
					</motion.div>
					<motion.div
						variants={slideLeft}
						initial="slideLeftInitial"
						animate={wasGetStartedSectionInView ? "slideLeft" : ""}
						custom={90}
						className="get-started-background-image get-started-background-image-cloud-2">
						<img src={cloud} alt="" />
					</motion.div>
					<motion.div
						variants={slideRight}
						initial="slideRightInitial"
						animate={wasGetStartedSectionInView ? "slideRight" : ""}
						custom={120}
						className="get-started-background-image get-started-background-image-cloud-3">
						<img src={cloud} alt="" />
					</motion.div>
				</div>
			</div>
			<div className="get-started-container">
				<div className="get-started-content">
					<motion.div
						className="get-started-content-title"
						variants={fadeInUp}
						initial="fadeInUpInitial"
						animate={wasGetStartedSectionInView ? "fadeInUp" : ""}
						custom={0.3}>
						Get started on your adventure in coding with Grasshopper.
					</motion.div>
					<div className="get-started-content-links">
						<motion.button
							variants={fadeInUp}
							initial="fadeInUpInitial"
							animate={wasGetStartedSectionInView ? "fadeInUp" : ""}
							custom={0.45}
							className="btn btn-primary btn-rounded btn-lg">
							{!isSmallerThanLaptop ? "Start coding today" : "Get Started"}
						</motion.button>
						<motion.a
							variants={fadeInUp}
							initial="fadeInUpInitial"
							animate={wasGetStartedSectionInView ? "fadeInUp" : ""}
							custom={0.6}
							href="https://play.google.com/store/apps/details?id=com.area120.grasshopper"
							className="btn google-play-btn">
							<img src={googlePlayButton} alt="" />
						</motion.a>
					</div>
				</div>
				<div></div>
			</div>
		</motion.section>
	);
}
