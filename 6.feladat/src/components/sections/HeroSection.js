import { motion } from "framer-motion";
import { heroSubtitleAnimation } from "../../animations/heroSubtitleAnimation";
import { hero } from "../../assets/images";
import "../../assets/sass/hero.scss";

const {
	backgroundHills,
	darkBush,
	lightBush,
	grasshopper,
	iphone,
	macbook,
	pixelPhone,
	darkTree,
	skinnyTree,
} = hero;

export default function HeroSection({ subtitleOptions }) {
	const extendedSubtitleOptions = Array(4).fill(subtitleOptions).flat();

	return (
		<section className="hero">
			<div className="hero-container">
				<h1 className="hero-title">
					Welcome to Grasshopper, the coding app for beginners
				</h1>
				<h2 className="hero-subtitle">
					Learning to code{" "}
					<span className="hero-subtitle-options">
						<motion.span
							variants={heroSubtitleAnimation}
							animate="subtitleAnimation"
							transition="subtitleTransition"
							className="hero-subtitle-options-wrapper">
							{extendedSubtitleOptions.map((option, index) => (
								<span
									className="hero-subtitle-options-option"
									key={`hero-subtitle-options-option-${index}`}>
									{option}
								</span>
							))}
						</motion.span>
					</span>
				</h2>
				<button className="btn btn-primary btn-lg btn-rounded">
					Start coding today
				</button>
			</div>
			<div className="hero-images">
				<div className="hero-images-image hero-images-image-backgroundHills">
					<img src={backgroundHills} alt="" />
				</div>
				<div className="hero-images-image hero-images-image-bush-dark">
					<img src={darkBush} alt="" />
				</div>
				<div className="hero-images-image hero-images-image-bush-light">
					<img src={lightBush} alt="" />
				</div>
				<div>
					<img
						src={grasshopper}
						alt=""
						className="hero-images-image hero-images-image-grasshopper"
					/>
				</div>
				<div className="hero-images-image hero-images-image-ios">
					<img src={iphone} alt="" />
				</div>
				<div className="hero-images-image hero-images-image-macbook">
					<img src={macbook} alt="" />
				</div>
				<div className="hero-images-image hero-images-image-pixel">
					<img src={pixelPhone} alt="" />
				</div>
				<div className="hero-images-image hero-images-image-tree-skinny">
					<img src={skinnyTree} alt="" />
				</div>
				<div className="hero-images-image hero-images-image-tree-dark">
					<img src={darkTree} alt="" />
				</div>
			</div>
		</section>
	);
}
