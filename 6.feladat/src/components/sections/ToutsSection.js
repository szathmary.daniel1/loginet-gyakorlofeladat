import { useRef } from "react";
import { motion, useInView } from "framer-motion";
import { fadeInUp } from "../../animations/fadeAnimations";
import { touts } from "../../assets/images";
import "../../assets/sass/touts.scss";

export default function Touts() {
	const toutsSectionRef = useRef();

	const isToutsSectionInView = useInView(toutsSectionRef, {once: true});

	return (
		<motion.section className="touts" ref={toutsSectionRef}>
			<div className="touts-container">
				<motion.h2
					variants={fadeInUp}
					initial="fadeInUpInitial"
					animate={isToutsSectionInView ? "fadeInUp" : ""}
					custom={0.15}
					className="section-title">
					The best way to start your coding adventure.
				</motion.h2>
				<div className="tout-items">
					<motion.div
						variants={fadeInUp}
						initial="fadeInUpInitial"
						animate={isToutsSectionInView ? "fadeInUp" : ""}
						custom={0.3}
						className="tout-items-item">
						<div className="tout-items-item-image">
							<img src={touts.waving} alt="" />
						</div>
						<div className="tout-items-item-body">
							Learn with fun, quick lessons on your phone that teach you to
							write real JavaScript.
						</div>
					</motion.div>
					<motion.div
						variants={fadeInUp}
						initial="fadeInUpInitial"
						animate={isToutsSectionInView ? "fadeInUp" : ""}
						custom={0.45}
						className="tout-items-item">
						<div className="tout-items-item-image">
							<img src={touts.coding} alt="" />
						</div>
						<div className="tout-items-item-body">
							Move through progressively challenging levels as you develop your
							abilities.
						</div>
					</motion.div>
					<motion.div
						variants={fadeInUp}
						initial="fadeInUpInitial"
						animate={isToutsSectionInView ? "fadeInUp" : ""}
						custom={0.6}
						className="tout-items-item">
						<div className="tout-items-item-image">
							<img src={touts.journey} alt="" />
						</div>
						<div className="tout-items-item-body">
							Graduate with fundamental programming skills for your next step as
							a coder.
						</div>
					</motion.div>
				</div>
			</div>
		</motion.section>
	);
}
