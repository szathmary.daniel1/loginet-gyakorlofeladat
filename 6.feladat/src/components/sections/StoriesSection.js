import { useContext, useEffect, useRef, useState } from "react";
import { motion, useInView, useMotionTemplate, useSpring } from "framer-motion";
import { MediaQueryContext } from "../../contexts/MediaQueryContext";
import useInterval from "../../hooks/useInterval";
import { fadeIn, fadeInUp } from "../../animations/fadeAnimations";
import "../../assets/sass/stories.scss";

export default function StoriesSection({ stories }) {
	const { isMobile } = useContext(MediaQueryContext);

	const storiesSectionRef = useRef();

	const wasStoriesSectionInView = useInView(storiesSectionRef, { once: true });

	const storiesTrackX = useSpring(isMobile ? -16 : -10, { duration: 2000 });
	const storiesTrackTransformX = useMotionTemplate`translateX(${storiesTrackX}%)`;

	const [displayedStoryIndex, setDisplayedStoryIndex] = useState(0);

	const { startInterval, resetInterval } = useInterval(
		() =>
			setDisplayedStoryIndex((oldIndex) =>
				oldIndex + 1 >= stories.length ? 0 : oldIndex + 1
			),
		3000
	);

	useEffect(
		() => {
			startInterval();
		},
		[] //eslint-disable-line
	);

	const extendedStories = [...stories];
	extendedStories.unshift(stories[stories.length - 1]);
	extendedStories.unshift(stories[stories.length - 2]);
	extendedStories.push(stories[0]);
	extendedStories.push(stories[1]);

	useEffect(
		() => {
			storiesTrackX.set((isMobile ? -16 : -10) - displayedStoryIndex * 12.5);
		},
		[displayedStoryIndex] //eslint-disable-line
	);

	const handleSelectDisplayedStory = (newIndex) => {
		setDisplayedStoryIndex(newIndex);
		resetInterval();
	};

	return (
		<section className="stories" ref={storiesSectionRef}>
			<motion.div
				variants={fadeInUp}
				initial="fadeInUpInitial"
				animate="fadeInUp"
				custom={0.15}
				className="section-title">
				Grasshopper Stories
			</motion.div>
			<div className="stories-container">
				<div className="stories-track-container">
					<motion.div
						className="stories-track"
						variants={fadeIn}
						initial="fadeInInitial"
						animate={wasStoriesSectionInView ? "fadeIn" : ""}
						custom={0.15}
						style={{ transform: storiesTrackTransformX }}>
						{extendedStories.map((story, index) => (
							<motion.div
								initial={{ opacity: 0.4 }}
								animate={
									index === displayedStoryIndex + 1
										? { opacity: 1, transition: { delay: 2 } }
										: {}
								}
								className="stories-story"
								key={`stories-story-${index}`}>
								<div className="stories-story-container">
									<div className="stories-story-image">
										<img src={story.profilePicture} alt="" />
									</div>
									<div className="stories-story-content">
										<div className="stories-story-body">{story.story}</div>
										<div className="stories-story-name">{story.name}</div>
									</div>
								</div>
							</motion.div>
						))}
					</motion.div>
				</div>
				<div className="dots">
					{Array(4)
						.fill(0)
						.map((_, index) => (
							<button
								className={
									"btn btn-md dots-dot " +
									(index === displayedStoryIndex ? "active" : "")
								}
								key={`dot-${index}`}
								onClick={() => handleSelectDisplayedStory(index)}></button>
						))}
				</div>
			</div>
		</section>
	);
}
