import { useContext, useRef } from "react";
import { motion, useInView } from "framer-motion";
import { MediaQueryContext } from "../../contexts/MediaQueryContext";
import { slideLeft, slideRight } from "../../animations/slideAnimations";
import "../../assets/sass/concepts.scss";

export default function ConceptsSection({ concepts }) {
	const { isLaptop, isDesktop } = useContext(MediaQueryContext);
	
	const conceptsSectionRef = useRef();

	const wasConceptsSectionInView = useInView(conceptsSectionRef, {
		once: true,
	});
	const isSmallerThanLaptop = !isLaptop && !isDesktop;

	const row1 = concepts.slice(0, 18);
	const row2 = concepts.slice(18, 35);
	const row3 = concepts.slice(35, concepts.length);

	const row1Mobile = [...concepts.slice(0, 11), "And more..."];

	return (
		<section className="concepts" ref={conceptsSectionRef}>
			<div className="concepts-container">
				<div className="section-title">
					Learn concepts that apply to any programming language
				</div>
				{isSmallerThanLaptop ? (
					<div className="concepts-rows mobile">
						<div className="concepts-rows-row mobile">
							{row1Mobile.map((item, index) => (
								<p className="concepts-rows-row-item" key={index}>
									{item}
								</p>
							))}
						</div>
					</div>
				) : (
					<div className="concepts-rows">
						<motion.div
							className="concepts-rows-row"
							variants={slideLeft}
							animate={wasConceptsSectionInView ? "slideLeftQuarter" : ""}
							initial="slideLeftInitial"
							custom={135}>
							{row1.map((item, index) => (
								<p className="concepts-rows-row-item" key={index}>
									{item}
								</p>
							))}
						</motion.div>
						<motion.div
							variants={slideRight}
							animate={wasConceptsSectionInView ? "slideRightQuarter" : ""}
							initial="slideRightInitial"
							custom={180}
							className="concepts-rows-row">
							{row2.map((item, index) => (
								<p className="concepts-rows-row-item" key={index}>
									{item}
								</p>
							))}
						</motion.div>
						<motion.div
							variants={slideLeft}
							animate={wasConceptsSectionInView ? "slideLeftQuarter" : ""}
							initial="slideLeftInitial"
							custom={210}
							className="concepts-rows-row">
							{row3.map((item, index) => (
								<p className="concepts-rows-row-item" key={index}>
									{item}
								</p>
							))}
						</motion.div>
					</div>
				)}
			</div>
		</section>
	);
}
