import { useRef } from "react";
import { motion, useInView } from "framer-motion";
import { fadeInUp } from "../../animations/fadeAnimations";
import devices from "../../assets/images/home-learn-to-code/devices.svg";
import grasshopperMeditate from "../../assets/images/home-learn-to-code/grasshopper__practice__meditate.svg";
import "../../assets/sass/learnToCode.scss";

export default function LearnToCodeSection() {
	const learnToCodeSectionRef = useRef();

	const isLearnToCodeSectionInView = useInView(learnToCodeSectionRef, {
		once: true,
	});

	return (
		<motion.section className="learn-to-code" ref={learnToCodeSectionRef}>
			<div className="learn-to-code-container">
				<motion.h2
					variants={fadeInUp}
					initial="fadeInUpInitial"
					animate={isLearnToCodeSectionInView ? "fadeInUp" : ""}
					custom={0.15}
					className="section-title">
					Learn to code everywhere
				</motion.h2>
				<motion.div
					variants={fadeInUp}
					initial="fadeInUpInitial"
					animate={isLearnToCodeSectionInView ? "fadeInUp" : ""}
					custom={0.3}
					className="learn-to-code-body">
					Grasshopper is available on Android and all web browsers. Your
					progress syncs seamlessly between devices.
				</motion.div>
			</div>
			<div className="learn-to-code-images">
				<div className="learn-to-code-images-container">
					<motion.div
						className="learn-to-code-images-image"
						variants={fadeInUp}
						initial="fadeInUpInitial"
						animate={isLearnToCodeSectionInView ? "fadeInUp" : ""}
						custom={0.6}>
						<img src={grasshopperMeditate} alt="" />
					</motion.div>
					<motion.div
						className="learn-to-code-images-image"
						variants={fadeInUp}
						initial="fadeInUpInitial"
						animate={isLearnToCodeSectionInView ? "fadeInUp" : ""}
						custom={0.6}>
						<img src={devices} alt="" />
					</motion.div>
				</div>
			</div>
		</motion.section>
	);
}
