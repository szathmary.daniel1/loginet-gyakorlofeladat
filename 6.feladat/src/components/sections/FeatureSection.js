import { useContext, useEffect, useRef, useState } from "react";
import { motion, useInView } from "framer-motion";
import useInterval from "../../hooks/useInterval";
import { MediaQueryContext } from "../../contexts/MediaQueryContext";
import { backgroundSquaresAnimationVariant } from "../../animations/backgroundSquaresAnimation";
import { fadeIn, fadeInLeft } from "../../animations/fadeAnimations";
import backgroundSquares from "../../data/backgroundSquares";
import "../../assets/sass/features.scss";

export default function FeatureSection({ features }) {
	const { isDesktop } = useContext(MediaQueryContext);
	
	const [displayedFeatureIndex, setDisplayedFeatureIndex] = useState(0);
	const featureSectionRef = useRef();

	const isFeatureSectionInView = useInView(featureSectionRef);
	const isFeatureSectionInViewOnce = useInView(featureSectionRef, {
		once: true,
	});


	const { resetInterval, startInterval } = useInterval(() => {
		setDisplayedFeatureIndex((oldIndex) =>
			oldIndex + 1 >= featuresLength ? 0 : oldIndex + 1
		);
	}, 3000);

	const featuresLength = features.length;
	const displayedFeature = features[displayedFeatureIndex];

	useEffect(
		() => {
			startInterval();
		},
		[] // eslint-disable-line
	);

	const handleSelectDisplayedFeature = (newIndex) => {
		if (newIndex < 0 || newIndex >= featuresLength) return;

		setDisplayedFeatureIndex(newIndex);
		resetInterval();
	};

	return (
		<motion.section className="features" ref={featureSectionRef}>
			<div className="features-container">
				{!isDesktop ? (
					<div className="features-list">
						{features.map((feature, index) => (
							<div className="features-list-item" key={index}>
								<div className="features-list-item-body">
									<div className="features-list-item-icon">
										<img src={feature.icon} alt="" />
									</div>
									<div className="features-list-item-title">
										{feature.title}
									</div>
								</div>
								<div
									className={
										"features-list-item-image " + (index === 1 ? "large" : "")
									}>
									<img src={feature.image} alt="" />
								</div>
							</div>
						))}
					</div>
				) : (
					<>
						<div className="features-carousel">
							<div className="features-carousel-body">
								<motion.div
									key={displayedFeatureIndex}
									variants={fadeInLeft}
									className="features-carousel-body-title"
									initial={"fadeInLeftInitial"}
									animate={isFeatureSectionInView ? "fadeInLeft" : ""}>
									<div className="features-carousel-body-title-icon">
										<img src={displayedFeature.icon} alt="" />
									</div>
									<div className="features-carousel-body-title-text">
										{displayedFeature.title}
									</div>
								</motion.div>
								<div className="dots">
									{Array(featuresLength)
										.fill(0)
										.map((_, index) => (
											<button
												className={
													"btn btn-md dots-dot " +
													(index === displayedFeatureIndex ? "active" : "")
												}
												key={`dot-${index}`}
												onClick={() =>
													handleSelectDisplayedFeature(index)
												}></button>
										))}
								</div>
							</div>
							<motion.div
								key={displayedFeatureIndex}
								className="features-carousel-image"
								variants={fadeIn}
								initial="fadeInInitial"
								animate={isFeatureSectionInView ? "fadeIn" : ""}>
								<img src={displayedFeature.slideImage} alt="" />
							</motion.div>
						</div>
					</>
				)}
			</div>
			<div className="features-background">
				<div className="features-background-container">
					<motion.svg
						variants={backgroundSquaresAnimationVariant}
						initial="rotationInitial"
						animate={isFeatureSectionInView ? "rotate" : ""}
						custom={{ deg: -25, delay: 0.15 }}
						className="features-background-square features-background-square-1"
						width="90px"
						height="90px"
						viewBox="0 0 50 50">
						<rect
							x="0.77398322"
							y="0.397442376"
							width="50"
							height="50"
							fill="#c3f6a2"></rect>
					</motion.svg>
					<motion.svg
						variants={backgroundSquaresAnimationVariant}
						initial="rotationInitial"
						animate={isFeatureSectionInView ? "rotate" : ""}
						custom={{ deg: 35, delay: 0.3 }}
						className="features-background-square features-background-square-2"
						width="40px"
						height="40px"
						viewBox="0 0 50 50">
						<rect
							x="0.77398322"
							y="0.397442376"
							width="50"
							height="50"
							fill="rgba(50,243,150,0.6)"></rect>
					</motion.svg>
					{backgroundSquares.map((square, index) => (
						<motion.svg
							variants={backgroundSquaresAnimationVariant}
							initial="rotationInitial"
							animate={isFeatureSectionInViewOnce ? "rotate" : ""}
							custom={{ deg: square.deg, delay: square.delay }}
							className={`features-background-square features-background-square-${
								index + 3
							}`}
							width={square.width}
							height={square.height}
							viewBox="0 0 50 50"
							key={`square-${index}`}>
							<rect
								x="0.77398322"
								y="0.397442376"
								width="50"
								height="50"
								fill={square.fill}></rect>
						</motion.svg>
					))}
					<motion.svg
						variants={backgroundSquaresAnimationVariant}
						initial="rotationInitial"
						animate={isFeatureSectionInViewOnce ? "rotate" : ""}
						custom={{ deg: -15, delay: 1.8 }}
						width="212px"
						height="212px"
						viewBox="0 0 190 190"
						className="features-background-square features-background-square-group features-background-square-group-1">
						<g
							id="Group-4-Copy-2"
							stroke="none"
							strokeWidth="1"
							fill="none"
							fillRule="evenodd">
							<rect
								id="Rectangle"
								stroke="#B9B9B9"
								strokeWidth="2"
								transform="translate(95.784724, 95.784724) rotate(-360.000000) translate(-95.784724, -95.784724) "
								x="8.4625948"
								y="8.4625948"
								width="174.644258"
								height="174.644258"></rect>
							<rect
								fill="#4A4A4A"
								fillRule="evenodd"
								stroke="#4A4A4A"
								strokeWidth="2"
								x="1"
								y="1"
								width="17.2335861"
								height="17.2335861"></rect>
							<rect
								stroke="#4A4A4A"
								strokeWidth="2"
								fill="#4A4A4A"
								fillRule="evenodd"
								x="1"
								y="173.335861"
								width="17.2335861"
								height="17.2335861"></rect>
							<rect
								fill="#4A4A4A"
								fillRule="evenodd"
								stroke="#4A4A4A"
								strokeWidth="2"
								x="173.335861"
								y="1"
								width="17.2335861"
								height="17.2335861"></rect>
							<rect
								fill="#4A4A4A"
								fillRule="evenodd"
								stroke="#4A4A4A"
								strokeWidth="2"
								x="173.335861"
								y="173.335861"
								width="17.2335861"
								height="17.2335861"></rect>
						</g>
					</motion.svg>
					<motion.svg
						variants={backgroundSquaresAnimationVariant}
						initial="rotationInitial"
						animate={isFeatureSectionInViewOnce ? "rotate" : ""}
						custom={{ deg: 45, delay: 1.95 }}
						width="190px"
						height="190px"
						viewBox="0 0 190 190"
						className="features-background-square features-background-square-group features-background-square-group-2">
						<g
							id="Group-4-Copy-2"
							stroke="none"
							strokeWidth="1"
							fill="none"
							fillRule="evenodd">
							<rect
								id="Rectangle"
								stroke="#B9B9B9"
								strokeWidth="2"
								transform="translate(95.784724, 95.784724) rotate(-360.000000) translate(-95.784724, -95.784724) "
								x="8.4625948"
								y="8.4625948"
								width="174.644258"
								height="174.644258"></rect>
							<rect
								fill="#4A4A4A"
								fillRule="evenodd"
								stroke="#4A4A4A"
								strokeWidth="2"
								x="1"
								y="1"
								width="17.2335861"
								height="17.2335861"></rect>
							<rect
								stroke="#4A4A4A"
								strokeWidth="2"
								fill="#4A4A4A"
								fillRule="evenodd"
								x="1"
								y="173.335861"
								width="17.2335861"
								height="17.2335861"></rect>
							<rect
								fill="#4A4A4A"
								fillRule="evenodd"
								stroke="#4A4A4A"
								strokeWidth="2"
								x="173.335861"
								y="1"
								width="17.2335861"
								height="17.2335861"></rect>
							<rect
								fill="#4A4A4A"
								fillRule="evenodd"
								stroke="#4A4A4A"
								strokeWidth="2"
								x="173.335861"
								y="173.335861"
								width="17.2335861"
								height="17.2335861"></rect>
						</g>
					</motion.svg>
				</div>
			</div>
		</motion.section>
	);
}
