import footerLogo from "../assets/images/common/code-with-google.v2.png";
import "../assets/sass/footer.scss";

export default function Footer() {
	return (
		<footer className="footer">
			<div className="footer-container">
				<div className="footer-content">
					<nav className="footer-links">
						<ul className="footer-links-list">
							<li className="footer-links-list-item">Home</li>
							<li className="footer-links-list-item">What is coding</li>
							<li className="footer-links-list-item">About</li>
						</ul>
						<ul className="footer-links-list">
							<li className="footer-links-list-item">Email us</li>
							<li className="footer-links-list-item">FAQ</li>
						</ul>
						<ul className="footer-links-list">
							<li className="footer-links-list-item">Terms of service</li>
							<li className="footer-links-list-item">Privacy policy</li>
						</ul>
					</nav>
					<div className="footer-logo">
						<img src={footerLogo} alt="" />
					</div>
				</div>
			</div>
		</footer>
	);
}
