import backgroundHills from "./hero/hero__background-hills.svg";
import darkBush from "./hero/hero__bush-dark.svg";
import lightBush from "./hero/hero__bush-light.svg";
import grasshopper from "./hero/hero__grasshopper.svg";
import iphone from "./hero/hero__ios.png";
import macbook from "./hero/hero__macbook.png";
import pixelPhone from "./hero/hero__pixel.png";
import darkTree from "./hero/hero__tree-dark.svg";
import skinnyTree from "./hero/hero__tree-skinny.svg";

import toutsCoding from "./home-touts/adventure__coding.svg";
import toutsJourney from "./home-touts/adventure__journey.svg";
import toutsWaving from "./home-touts/adventure__waving.svg";

import storiesUserProfile1 from "./home-stories/user1.jpg";
import storiesUserProfile2 from "./home-stories/user2.jpg";
import storiesUserProfile3 from "./home-stories/user3.jpg";
import storiesUserProfile4 from "./home-stories/user4.jpg";

import featureVisualPuzzlesIcon from "../images/home-features/carousel-icons/icon1_visual_puzzles.png";
import featureIndustryStandardIcon from "../images/home-features/carousel-icons/icon2_industry_standard.png";
import featureFeedbackIcon from "../images/home-features/carousel-icons/icon3_feedback.png";
import featureAchievementsIcon from "../images/home-features/carousel-icons/icon4_achievements.png";
import featureLaptopTabletIcon from "../images/home-features/carousel-icons/icon5_laptop_tablet.png";

import featureSlide1 from "./home-features/slide__device-screen-1.png";
import featureSlide2 from "./home-features/slide__device-screen-laptop.png";
import featureSlide3 from "./home-features/slide__device-screen-2.png";
import featureSlide4 from "./home-features/slide__device-screen-3.png";
import featureSlide5 from "./home-features/slide__device-screen-4.png";

import featureImage1 from "./common/feature__android-1.png";
import featureImage2 from "./common/feature__laptop.png";
import featureImage3 from "./common/feature__android-2.png";
import featureImage4 from "./common/feature__android-3.png";
import featureImage5 from "./common/feature__android-4.png";

export const hero = {
	backgroundHills,
	darkBush,
	lightBush,
	grasshopper,
	iphone,
	macbook,
	pixelPhone,
	darkTree,
	skinnyTree,
};

export const touts = {
	coding: toutsCoding,
	journey: toutsJourney,
	waving: toutsWaving,
};

export const userProfiles = [
	storiesUserProfile1,
	storiesUserProfile2,
	storiesUserProfile3,
	storiesUserProfile4,
];

export const featureIcons = {
	visualPuzzles: featureVisualPuzzlesIcon,
	industryStandard: featureIndustryStandardIcon,
	feedback: featureFeedbackIcon,
	achievements: featureAchievementsIcon,
	laptopTablet: featureLaptopTabletIcon,
};

export const featureSlideImages = [
	featureSlide1,
	featureSlide2,
	featureSlide3,
	featureSlide4,
	featureSlide5,
];

export const featureImages = [
	featureImage1,
	featureImage2,
	featureImage3,
	featureImage4,
	featureImage5,
];
