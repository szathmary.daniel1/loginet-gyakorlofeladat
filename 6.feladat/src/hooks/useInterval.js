import { useCallback, useEffect, useRef } from "react";

export default function useInterval(callback, delay) {
	const callbackRef = useRef(callback);
	const intervalIdRef = useRef();

	useEffect(() => {
		callbackRef.current = callback;
	}, [callback]);

	const startInterval = useCallback(() => {
		if (delay !== 0 && !!delay) {
			intervalIdRef.current = setInterval(callbackRef.current, delay);
		}
	}, [delay]);

	useEffect(() => {
		const id = intervalIdRef.current;
		return () => {
			clearInterval(id);
		};
	}, []);

	const resetInterval = useCallback(() => {
		clearInterval(intervalIdRef.current);
		startInterval();
	}, [startInterval]);

	return {
		resetInterval,
		startInterval,
	};
}
